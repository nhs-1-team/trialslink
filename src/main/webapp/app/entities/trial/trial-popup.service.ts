import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Trial } from './trial.model';
import { TrialService } from './trial.service';
@Injectable()
export class TrialPopupService {
    private isOpen = false;
    constructor (
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private trialService: TrialService

    ) {}

    open (component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.trialService.find(id).subscribe(trial => {
                trial.recordVerificationDate = this.datePipe
                    .transform(trial.recordVerificationDate, 'yyyy-MM-ddThh:mm');
                trial.startDate = this.datePipe
                    .transform(trial.startDate, 'yyyy-MM-ddThh:mm');
                trial.primaryCompletionDate = this.datePipe
                    .transform(trial.primaryCompletionDate, 'yyyy-MM-ddThh:mm');
                trial.studyCompletionDate = this.datePipe
                    .transform(trial.studyCompletionDate, 'yyyy-MM-ddThh:mm');
                this.trialModalRef(component, trial);
            });
        } else {
            return this.trialModalRef(component, new Trial());
        }
    }

    trialModalRef(component: Component, trial: Trial): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.trial = trial;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
