import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Trial } from './trial.model';
import { TrialPopupService } from './trial-popup.service';
import { TrialService } from './trial.service';

@Component({
    selector: 'jhi-trial-delete-dialog',
    templateUrl: './trial-delete-dialog.component.html'
})
export class TrialDeleteDialogComponent {

    trial: Trial;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private trialService: TrialService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.trialService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'trialListModification',
                content: 'Deleted an trial'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trial-delete-popup',
    template: ''
})
export class TrialDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private trialPopupService: TrialPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.trialPopupService
                .open(TrialDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
