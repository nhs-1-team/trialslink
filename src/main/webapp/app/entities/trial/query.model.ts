export class Query {
    constructor(public locations?: Array<string>,
                public conditions?: Array<string>,
                public genders?: Array<string>,
                public statuses?: Array<string>,
                public phases?: Array<string>,
                public types?: Array<string>,
                public conditionStatuses?: Array<string>,
                public age?: number) {
        this.conditions = [];
        this.locations = [];
        this.genders = [];
        this.statuses = [];
        this.phases = [];
        this.types = [];
        this.conditionStatuses = [];
    }
}
