import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Trial } from './trial.model';
import { TrialCentre } from '../trial-centre/trial-centre.model';
import { DateUtils } from 'ng-jhipster';
@Injectable()
export class TrialService {

    private resourceUrl = 'api/trials';
    private shortNameUrl = 'api/trials/shortName';
    private resourceSearchUrl = 'api/_search/trials';
    private omniSearchUrl = 'api/trials/omni_search';
    private resourceIndexUrl = 'api/_index/trials';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(trial: Trial): Observable<Trial> {
        let copy: Trial = Object.assign({}, trial);
        copy.recordVerificationDate = this.dateUtils.toDate(trial.recordVerificationDate);
        copy.startDate = this.dateUtils.toDate(trial.startDate);
        copy.primaryCompletionDate = this.dateUtils.toDate(trial.primaryCompletionDate);
        copy.studyCompletionDate = this.dateUtils.toDate(trial.studyCompletionDate);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(trial: Trial): Observable<Trial> {
        let copy: Trial = Object.assign({}, trial);

        copy.recordVerificationDate = this.dateUtils.toDate(trial.recordVerificationDate);

        copy.startDate = this.dateUtils.toDate(trial.startDate);

        copy.primaryCompletionDate = this.dateUtils.toDate(trial.primaryCompletionDate);

        copy.studyCompletionDate = this.dateUtils.toDate(trial.studyCompletionDate);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Trial> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            let jsonResponse = res.json();
            jsonResponse.recordVerificationDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.recordVerificationDate);
            jsonResponse.startDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.startDate);
            jsonResponse.primaryCompletionDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.primaryCompletionDate);
            jsonResponse.studyCompletionDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.studyCompletionDate);
            jsonResponse.createdDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.createdDate);
            jsonResponse.lastModifiedDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.lastModifiedDate);
            return jsonResponse;
        });
    }

    import(id: string): Observable<Trial> {
        return this.http.get(`${this.resourceUrl}/${id}/import`).map((res: Response) => {
            let jsonResponse = res.json();
            jsonResponse.recordVerificationDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.recordVerificationDate);
            jsonResponse.startDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.startDate);
            jsonResponse.primaryCompletionDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.primaryCompletionDate);
            jsonResponse.studyCompletionDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.studyCompletionDate);
            jsonResponse.createdDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.createdDate);
            jsonResponse.lastModifiedDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.lastModifiedDate);
            return jsonResponse;
        });
    }

    centres(id: number): Observable<Response> {
        return this.http.get(`${this.resourceUrl}/${id}/centres`)
            .map((res: any) => this.convertResponse(res));
    }

    addCentre(id: string, centre: TrialCentre): Observable<TrialCentre> {
        return this.http.post(`${this.resourceUrl}/${id}/centres`, centre).map((res: Response) => {
            let jsonResponse = res.json();
            jsonResponse.startDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.startDate);
            jsonResponse.endDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.endDate);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res))
        ;
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    indexAllTrials(): Observable<Response> {
        return this.http.get(`${this.resourceIndexUrl}`);
    }

    search(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.post(this.resourceSearchUrl, req.query, options)
            .map((res: any) => this.convertResponse(res))
            ;
    }

    findByShortName(shortName: any): Observable<Response> {
        return this.http.get(`${this.shortNameUrl}/${shortName}`);
    }

    omniSearch(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.omniSearchUrl, options)
            .map((res: any) => this.convertResponse(res))
            ;
    }

    private convertResponse(res: any): any {
        let jsonResponse = res.json();
        if (res.json().results !== undefined) {
            jsonResponse = jsonResponse.results;
        }
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].recordVerificationDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].recordVerificationDate);
            jsonResponse[i].startDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].startDate);
            jsonResponse[i].primaryCompletionDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].primaryCompletionDate);
            jsonResponse[i].studyCompletionDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].studyCompletionDate);
            jsonResponse[i].createdDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].createdDate);
            jsonResponse[i].lastModifiedDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].lastModifiedDate);
        }
        if (res.json().results !== undefined) {
            res.json().results = jsonResponse;
        } else {
            res._body = jsonResponse;
        }
        return res;
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        let options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            let params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            params.set('q', req.query);

            options.search = params;
        }
        return options;
    }
}
