import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import { TrialCentreListComponent } from '../trial-centre/trial-centre-list.component';
import { ContactListComponent } from '../contact/contact-list.component';
import { IdInfoEmbeddedComponent } from '../id-info/id-info-embedded.component';
import { CharacteristicListComponent } from '../characteristic/characteristic-list.component';
import { InterventionListComponent } from '../intervention/intervention-list.component';
import { ConditionListComponent } from '../condition/condition-list.component';
import { ConditionDetailComponent } from '../condition/condition-detail.component';

import {
    TrialService,
    TrialPopupService,
    TrialComponent,
    TrialDetailComponent,
    TrialDialogComponent,
    TrialPopupComponent,
    TrialDeletePopupComponent,
    TrialDeleteDialogComponent,
    trialRoute,
    trialPopupRoute,
} from './';

let ENTITY_STATES = [
    ...trialRoute,
    ...trialPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TrialComponent,
        TrialDetailComponent,
        TrialDialogComponent,
        TrialDeleteDialogComponent,
        TrialPopupComponent,
        TrialDeletePopupComponent,
        TrialCentreListComponent,
        ContactListComponent,
        IdInfoEmbeddedComponent,
        CharacteristicListComponent,
        InterventionListComponent,
        ConditionListComponent,
        ConditionDetailComponent
    ],
    entryComponents: [
        TrialComponent,
        TrialDialogComponent,
        TrialPopupComponent,
        TrialDeleteDialogComponent,
        TrialDeletePopupComponent,
    ],
    providers: [
        TrialService,
        TrialPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkTrialModule {}
