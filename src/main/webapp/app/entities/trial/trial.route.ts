import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { TrialComponent } from './trial.component';
import { TrialDetailComponent } from './trial-detail.component';
import { TrialPopupComponent } from './trial-dialog.component';
import { TrialDeletePopupComponent } from './trial-delete-dialog.component';

import { Principal } from '../../shared';


export const trialRoute: Routes = [
  {
    path: 'trial',
    component: TrialComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trial.home.title'
    }
  }, {
    path: 'trial/:id',
    component: TrialDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trial.home.title'
    }
  }
];

export const trialPopupRoute: Routes = [
  {
    path: 'trial-new',
    component: TrialPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trial.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'trial/:id/edit',
    component: TrialPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trial.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'trial/:id/delete',
    component: TrialDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trial.home.title'
    },
    outlet: 'popup'
  }
];
