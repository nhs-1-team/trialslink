import { Trial } from '../trial';
import { Contact } from '../contact';
import { Organisation } from '../organisation/organisation.model';
import { Day } from './day.model';

export class TrialCentre extends Organisation {
    constructor(
        public id?: string,
        public name?: string,
        public allDay?: boolean,
        public startDate?: any,
        public endDate?: any,
        public days?: Day[],
        public trialId?: string,
        public contacts?: Contact[]
    ) {
        super(name, id);
        this.allDay = false;
        this.days = [];
    }
}
