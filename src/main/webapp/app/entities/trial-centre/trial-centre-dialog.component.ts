import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { TrialCentre } from './trial-centre.model';
import { Day } from './day.model';
import { TrialCentrePopupService } from './trial-centre-popup.service';
import { TrialCentreService } from './trial-centre.service';
import { OrganisationService } from '../organisation/organisation.service';

@Component({
    selector: 'jhi-trial-centre-dialog',
    templateUrl: './trial-centre-dialog.component.html'
})
export class TrialCentreDialogComponent implements OnInit {

    trialCentre: TrialCentre;
    authorities: any[];
    locations: Array<string>;
    isSaving: boolean;
    knownDays: Array<string>;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private trialCentreService: TrialCentreService,
        private eventManager: EventManager,
        private organisationService: OrganisationService
    ) {
        this.jhiLanguageService.setLocations(['trialCentre', 'trialStatus']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.organisationService.allAsFilter({
                size: 500
            }
        ).subscribe(
            (res: Response) => {
                this.locations = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
        this.knownDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    selectedLocation(value: any): void {
        this.trialCentre.name = value.text;
    }

    selectedDay(value: any): void {
        this.trialCentre.days.push(value.id);
    }

    removedDay(value: any): void {
        this.trialCentre.days = this.trialCentre.days.filter(function(v){return v !== value.id; });
    }

    save () {
        this.isSaving = true;
        if (this.trialCentre.id !== undefined) {
            this.trialCentreService.update(this.trialCentre)
                .subscribe((res: TrialCentre) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.trialCentreService.create(this.trialCentre)
                .subscribe((res: TrialCentre) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: TrialCentre) {
        this.eventManager.broadcast({ name: 'trialCentreListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-trial-centre-popup',
    template: ''
})
export class TrialCentrePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private trialCentrePopupService: TrialCentrePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.trialCentrePopupService
                    .open(TrialCentreDialogComponent, params['id'], false);
            } else {
                this.modalRef = this.trialCentrePopupService
                    .open(TrialCentreDialogComponent, params['trialId'], true);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
