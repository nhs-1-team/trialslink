import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { TrialCentre } from './trial-centre.model';
import { TrialCentreService } from './trial-centre.service';

@Component({
    selector: 'jhi-trial-centre-detail',
    templateUrl: './trial-centre-detail.component.html'
})
export class TrialCentreDetailComponent implements OnInit, OnDestroy {

    trialCentre: TrialCentre;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private trialCentreService: TrialCentreService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['trialCentre']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.trialCentreService.find(id).subscribe(trialCentre => {
            this.trialCentre = trialCentre;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
