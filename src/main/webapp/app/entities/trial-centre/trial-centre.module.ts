import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
// import { ContactListComponent } from '../contact/contact-list.component';

import {
    TrialCentreService,
    TrialCentrePopupService,
    TrialCentreComponent,
    TrialCentreDetailComponent,
    TrialCentreDialogComponent,
    TrialCentrePopupComponent,
    TrialCentreDeletePopupComponent,
    TrialCentreDeleteDialogComponent,
    trialCentreRoute,
    trialCentrePopupRoute,
} from './';

let ENTITY_STATES = [
    ...trialCentreRoute,
    ...trialCentrePopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TrialCentreComponent,
        TrialCentreDetailComponent,
        TrialCentreDialogComponent,
        TrialCentreDeleteDialogComponent,
        TrialCentrePopupComponent,
        TrialCentreDeletePopupComponent,
        // ContactListComponent
    ],
    entryComponents: [
        TrialCentreComponent,
        TrialCentreDialogComponent,
        TrialCentrePopupComponent,
        TrialCentreDeleteDialogComponent,
        TrialCentreDeletePopupComponent,
    ],
    providers: [
        TrialCentreService,
        TrialCentrePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkTrialCentreModule {}
