import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { SecondaryId } from './secondary-id.model';
import { SecondaryIdService } from './secondary-id.service';

@Component({
    selector: 'jhi-secondary-id-detail',
    templateUrl: './secondary-id-detail.component.html'
})
export class SecondaryIdDetailComponent implements OnInit, OnDestroy {

    secondaryId: SecondaryId;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private secondaryIdService: SecondaryIdService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['secondaryId']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.secondaryIdService.find(id).subscribe(secondaryId => {
            this.secondaryId = secondaryId;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
