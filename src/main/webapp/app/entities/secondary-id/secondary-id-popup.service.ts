import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SecondaryId } from './secondary-id.model';
import { SecondaryIdService } from './secondary-id.service';
@Injectable()
export class SecondaryIdPopupService {
    private isOpen = false;
    constructor (
        private modalService: NgbModal,
        private router: Router,
        private secondaryIdService: SecondaryIdService

    ) {}

    open (component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.secondaryIdService.find(id).subscribe(secondaryId => {
                this.secondaryIdModalRef(component, secondaryId);
            });
        } else {
            return this.secondaryIdModalRef(component, new SecondaryId());
        }
    }

    secondaryIdModalRef(component: Component, secondaryId: SecondaryId): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.secondaryId = secondaryId;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
