import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdPopupService } from './secondary-id-popup.service';
import { SecondaryIdService } from './secondary-id.service';

@Component({
    selector: 'jhi-secondary-id-delete-dialog',
    templateUrl: './secondary-id-delete-dialog.component.html'
})
export class SecondaryIdDeleteDialogComponent {

    secondaryId: SecondaryId;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private secondaryIdService: SecondaryIdService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['secondaryId']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.secondaryIdService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'secondaryIdListModification',
                content: 'Deleted an secondaryId'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-secondary-id-delete-popup',
    template: ''
})
export class SecondaryIdDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private secondaryIdPopupService: SecondaryIdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.secondaryIdPopupService
                .open(SecondaryIdDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
