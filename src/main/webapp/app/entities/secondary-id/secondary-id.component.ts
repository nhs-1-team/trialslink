import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, JhiLanguageService, AlertService } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdService } from './secondary-id.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-secondary-id',
    templateUrl: './secondary-id.component.html'
})
export class SecondaryIdComponent implements OnInit, OnDestroy {
secondaryIds: SecondaryId[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private secondaryIdService: SecondaryIdService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
        this.jhiLanguageService.setLocations(['secondaryId']);
    }

    loadAll() {
        this.secondaryIdService.query().subscribe(
            (res: Response) => {
                this.secondaryIds = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSecondaryIds();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: SecondaryId) {
        return item.id;
    }



    registerChangeInSecondaryIds() {
        this.eventSubscriber = this.eventManager.subscribe('secondaryIdListModification', (response) => this.loadAll());
    }


    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
