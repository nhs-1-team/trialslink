import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { SecondaryIdComponent } from './secondary-id.component';
import { SecondaryIdDetailComponent } from './secondary-id-detail.component';
import { SecondaryIdPopupComponent } from './secondary-id-dialog.component';
import { SecondaryIdDeletePopupComponent } from './secondary-id-delete-dialog.component';

import { Principal } from '../../shared';


export const secondaryIdRoute: Routes = [
  {
    path: 'secondary-id',
    component: SecondaryIdComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.secondaryId.home.title'
    }
  }, {
    path: 'secondary-id/:id',
    component: SecondaryIdDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.secondaryId.home.title'
    }
  }
];

export const secondaryIdPopupRoute: Routes = [
  {
    path: 'secondary-id-new',
    component: SecondaryIdPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.secondaryId.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'secondary-id/:id/edit',
    component: SecondaryIdPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.secondaryId.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'secondary-id/:id/delete',
    component: SecondaryIdDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.secondaryId.home.title'
    },
    outlet: 'popup'
  }
];
