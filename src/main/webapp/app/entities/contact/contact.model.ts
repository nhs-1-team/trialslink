import { Role } from '../role';

export class Contact {
    constructor(
        public id?: string,
        public phone?: string,
        public personName?: string,
        public email?: string,
        public role?: Role,
        public trialCentreId?: string
    ) {
        this.role = new Role();
    }
}
