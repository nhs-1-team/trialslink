import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { IdInfo } from './id-info.model';
import { IdInfoService } from './id-info.service';
import { Trial } from '../trial/trial.model';

@Component({
    selector: 'jhi-id-info-embedded',
    templateUrl: './id-info-embedded.component.html'
})
export class IdInfoEmbeddedComponent implements OnInit, OnDestroy {

    idInfo: IdInfo;
    @Input() trial: Trial;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private idInfoService: IdInfoService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['idInfo', 'trialCentre', 'trial']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.idInfo = this.trial.idInfo;
        });
    }

    load (id) {
        this.idInfoService.find(id).subscribe(idInfo => {
            this.idInfo = idInfo;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
