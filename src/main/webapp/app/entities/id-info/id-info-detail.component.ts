import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { IdInfo } from './id-info.model';
import { IdInfoService } from './id-info.service';

@Component({
    selector: 'jhi-id-info-detail',
    templateUrl: './id-info-detail.component.html'
})
export class IdInfoDetailComponent implements OnInit, OnDestroy {

    idInfo: IdInfo;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private idInfoService: IdInfoService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['idInfo']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.idInfoService.find(id).subscribe(idInfo => {
            this.idInfo = idInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
