import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';

import {
    IdInfoService,
    IdInfoPopupService,
    IdInfoComponent,
    IdInfoDetailComponent,
    IdInfoDialogComponent,
    IdInfoPopupComponent,
    IdInfoDeletePopupComponent,
    IdInfoDeleteDialogComponent,
    idInfoRoute,
    idInfoPopupRoute,
} from './';

let ENTITY_STATES = [
    ...idInfoRoute,
    ...idInfoPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        IdInfoComponent,
        IdInfoDetailComponent,
        IdInfoDialogComponent,
        IdInfoDeleteDialogComponent,
        IdInfoPopupComponent,
        IdInfoDeletePopupComponent,
    ],
    entryComponents: [
        IdInfoComponent,
        IdInfoDialogComponent,
        IdInfoPopupComponent,
        IdInfoDeleteDialogComponent,
        IdInfoDeletePopupComponent,
    ],
    providers: [
        IdInfoService,
        IdInfoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkIdInfoModule {}
