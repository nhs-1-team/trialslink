import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { IdInfo } from './id-info.model';
import { IdInfoPopupService } from './id-info-popup.service';
import { IdInfoService } from './id-info.service';
@Component({
    selector: 'jhi-id-info-dialog',
    templateUrl: './id-info-dialog.component.html'
})
export class IdInfoDialogComponent implements OnInit {

    idInfo: IdInfo;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private idInfoService: IdInfoService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['idInfo']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.idInfo.id !== undefined) {
            this.idInfoService.update(this.idInfo)
                .subscribe((res: IdInfo) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.idInfoService.create(this.idInfo)
                .subscribe((res: IdInfo) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: IdInfo) {
        this.eventManager.broadcast({ name: 'idInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-id-info-popup',
    template: ''
})
export class IdInfoPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private idInfoPopupService: IdInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.idInfoPopupService
                    .open(IdInfoDialogComponent, params['id']);
            } else {
                this.modalRef = this.idInfoPopupService
                    .open(IdInfoDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
