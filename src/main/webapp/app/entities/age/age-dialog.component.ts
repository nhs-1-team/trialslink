import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Age } from './age.model';
import { AgePopupService } from './age-popup.service';
import { AgeService } from './age.service';
@Component({
    selector: 'jhi-age-dialog',
    templateUrl: './age-dialog.component.html'
})
export class AgeDialogComponent implements OnInit {

    age: Age;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private ageService: AgeService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['age', 'ageUnit']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.age.id !== undefined) {
            this.ageService.update(this.age)
                .subscribe((res: Age) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.ageService.create(this.age)
                .subscribe((res: Age) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Age) {
        this.eventManager.broadcast({ name: 'ageListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-age-popup',
    template: ''
})
export class AgePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private agePopupService: AgePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.agePopupService
                    .open(AgeDialogComponent, params['id']);
            } else {
                this.modalRef = this.agePopupService
                    .open(AgeDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
