import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Age } from './age.model';
import { AgePopupService } from './age-popup.service';
import { AgeService } from './age.service';

@Component({
    selector: 'jhi-age-delete-dialog',
    templateUrl: './age-delete-dialog.component.html'
})
export class AgeDeleteDialogComponent {

    age: Age;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private ageService: AgeService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['age', 'ageUnit']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.ageService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'ageListModification',
                content: 'Deleted an age'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-age-delete-popup',
    template: ''
})
export class AgeDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private agePopupService: AgePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.agePopupService
                .open(AgeDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
