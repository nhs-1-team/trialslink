
const enum AgeUnit {
    'YEARS',
    'MONTHS',
    'WEEKS',
    'DAYS',
    'HOURS',
    'MINUTES',
    'NA'

};
export class Age {
    constructor(
        public id?: string,
        public value?: number,
        public unit?: AgeUnit
    ) {
    }
}
