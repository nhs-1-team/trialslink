import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Age } from './age.model';
import { AgeService } from './age.service';

@Component({
    selector: 'jhi-age-detail',
    templateUrl: './age-detail.component.html'
})
export class AgeDetailComponent implements OnInit, OnDestroy {

    age: Age;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private ageService: AgeService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['age', 'ageUnit']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.ageService.find(id).subscribe(age => {
            this.age = age;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
