import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionPopupService } from './condition-popup.service';
import { ConditionService } from './condition.service';
@Component({
    selector: 'jhi-condition-dialog',
    templateUrl: './condition-dialog.component.html'
})
export class ConditionDialogComponent implements OnInit {

    condition: Condition;
    authorities: any[];
    isSaving: boolean;

    constructor(public activeModal: NgbActiveModal,
                private jhiLanguageService: JhiLanguageService,
                private alertService: AlertService,
                private conditionService: ConditionService,
                private eventManager: EventManager) {
        this.jhiLanguageService.setLocations(['condition', 'trialCentre', 'trial']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.condition.id !== undefined) {
            this.conditionService.update(this.condition)
                .subscribe((res: Condition) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.conditionService.create(this.condition)
                .subscribe((res: Condition) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess(result: Condition) {
        this.eventManager.broadcast({name: 'conditionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
        this.eventManager.broadcast({ name: 'trialModification', content: 'OK'});
    }

    private onSaveError(error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-condition-popup',
    template: ''
})
export class ConditionPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private conditionPopupService: ConditionPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.conditionPopupService
                    .open(ConditionDialogComponent, params['id'], false);
            } else {
                this.modalRef = this.conditionPopupService
                    .open(ConditionDialogComponent, params['trialId'], true);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
