import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';

import {
    ConditionService,
    ConditionPopupService,
    ConditionComponent,
    ConditionDialogComponent,
    ConditionPopupComponent,
    ConditionDeletePopupComponent,
    ConditionDeleteDialogComponent,
    conditionRoute,
    conditionPopupRoute,
    } from './';

let ENTITY_STATES = [
    ...conditionRoute,
    ...conditionPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    ],
    declarations: [
        ConditionComponent,
        ConditionDialogComponent,
        ConditionDeleteDialogComponent,
        ConditionPopupComponent,
        ConditionDeletePopupComponent,
    ],
    entryComponents: [
        ConditionComponent,
        ConditionDialogComponent,
        ConditionPopupComponent,
        ConditionDeleteDialogComponent,
        ConditionDeletePopupComponent,
    ],
    providers: [
        ConditionService,
        ConditionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkConditionModule {
}
