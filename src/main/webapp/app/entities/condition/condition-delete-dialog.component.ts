import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionPopupService } from './condition-popup.service';
import { ConditionService } from './condition.service';

@Component({
    selector: 'jhi-condition-delete-dialog',
    templateUrl: './condition-delete-dialog.component.html'
})
export class ConditionDeleteDialogComponent {

    condition: Condition;

    constructor(private jhiLanguageService: JhiLanguageService,
                private conditionService: ConditionService,
                public activeModal: NgbActiveModal,
                private eventManager: EventManager) {
        this.jhiLanguageService.setLocations(['condition']);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.conditionService.delete(this.condition.trialId, id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'conditionListModification',
                content: 'Deleted an condition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-condition-delete-popup',
    template: ''
})
export class ConditionDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private conditionPopupService: ConditionPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.conditionPopupService
                .open(ConditionDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
