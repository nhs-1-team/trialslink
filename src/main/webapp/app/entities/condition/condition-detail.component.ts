import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Response } from '@angular/http';

import { ActivatedRoute } from '@angular/router';
import { EventManager, AlertService, JhiLanguageService} from 'ng-jhipster';
import { Condition } from './condition.model';
import { ConditionService } from './condition.service';

@Component({
    selector: 'jhi-condition-detail',
    templateUrl: './condition-detail.component.html'
})
export class ConditionDetailComponent implements OnInit, OnDestroy {

    @Input() condition: Condition;
    @Input() gridLayout: boolean;
    @Input() knownConditions: Condition[];
    @Input() knownStatuses: Condition[];
    private editing: boolean;
    private subscription: any;
    authorities: any[];
    isSaving: boolean;

    constructor(private jhiLanguageService: JhiLanguageService,
                private conditionService: ConditionService,
                private alertService: AlertService,
                private eventManager: EventManager,
                private route: ActivatedRoute) {
        this.jhiLanguageService.setLocations(['condition', 'trialCentre', 'trial']);
        this.editing = false;
    }

    ngOnInit() {
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        // this.subscription = this.route.params.subscribe(params => {
        //    this.load(params['id']);
        // });
    }

    onConditionSelect(item: any) {
        this.condition.label = item.text;
        this.condition.code = item.id;
        // this.condition.system = item.system;
    }

    onStatusSelect(item: any) {
        this.condition.status = item.text;
        this.condition.statusCode = item.id;
    }

    update() {
        this.isSaving = true;
        this.conditionService.update(this.condition)
            .subscribe((res: Condition) =>
                this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
    }

    private onSaveSuccess(result: Condition) {
        this.eventManager.broadcast({name: 'conditionListModification', content: 'OK'});
        this.isSaving = false;
        this.editing = false;
    }

    private onSaveError(error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

}
