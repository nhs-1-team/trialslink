import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Intervention } from './intervention.model';
import { InterventionService } from './intervention.service';

@Component({
    selector: 'jhi-intervention-detail',
    templateUrl: './intervention-detail.component.html'
})
export class InterventionDetailComponent implements OnInit, OnDestroy {

    intervention: Intervention;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private interventionService: InterventionService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['intervention']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.interventionService.find(id).subscribe(intervention => {
            this.intervention = intervention;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
