import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Intervention } from './intervention.model';
import { InterventionService } from './intervention.service';
@Injectable()
export class InterventionPopupService {
    private isOpen = false;
    constructor (
        private modalService: NgbModal,
        private router: Router,
        private interventionService: InterventionService

    ) {}

    open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (!createNew) {
            this.interventionService.find(id).subscribe(intervention => {
                this.interventionModalRef(component, intervention);
            });
        } else {
            let intervention = new Intervention();
            intervention.trialId = id;
            return this.interventionModalRef(component, intervention);
        }
    }

    interventionModalRef(component: Component, intervention: Intervention): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.intervention = intervention;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
