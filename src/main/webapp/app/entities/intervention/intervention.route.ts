import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { InterventionComponent } from './intervention.component';
import { InterventionDetailComponent } from './intervention-detail.component';
import { InterventionPopupComponent } from './intervention-dialog.component';
import { InterventionDeletePopupComponent } from './intervention-delete-dialog.component';

import { Principal } from '../../shared';


export const interventionRoute: Routes = [
  {
    path: 'intervention',
    component: InterventionComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.intervention.home.title'
    }
  }, {
    path: 'intervention/:id',
    component: InterventionDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.intervention.home.title'
    }
  }
];

export const interventionPopupRoute: Routes = [
  {
    path: 'intervention-new/:trialId/create',
    component: InterventionPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.intervention.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'intervention/:id/edit',
    component: InterventionPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.intervention.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'intervention/:id/delete',
    component: InterventionDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.intervention.home.title'
    },
    outlet: 'popup'
  }
];
