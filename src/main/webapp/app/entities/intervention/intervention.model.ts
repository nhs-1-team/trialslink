export class Intervention {
    constructor(
        public id?: string,
        public label?: string,
        public system?: string,
        public code?: string,
        public type?: string,
        public trialId?: string
    ) {
    }
}
