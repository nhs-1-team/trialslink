import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Intervention } from './intervention.model';
@Injectable()
export class InterventionService {

    private resourceUrl = 'api/interventions';
    private trialsResourceUrl = 'api/trials';

    constructor(private http: Http) { }

    create(intervention: Intervention): Observable<Intervention> {
        let copy: Intervention = Object.assign({}, intervention);
        if (intervention.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${intervention.trialId}/interventions`, copy).map((res: Response) => {
                return res.json();
            });
        } else {
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                return res.json();
            });
        }
    }

    update(intervention: Intervention): Observable<Intervention> {
        let copy: Intervention = Object.assign({}, intervention);
        return this.http.put(`${this.trialsResourceUrl}/${intervention.trialId}/interventions`, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Intervention> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        ;
    }

    interventionsForTrial(id: string, req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/interventions`, options)
            .map((res: any) => this.convertResponse(res, id))
            ;
    }


    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: any, id: string): any {
        let jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].trialId = id;
        }
        res._body = jsonResponse;
        return res;
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        let options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            let params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            params.set('query', req.query);

            options.search = params;
        }
        return options;
    }
}
