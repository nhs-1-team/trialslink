import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, JhiLanguageService, AlertService } from 'ng-jhipster';

import { Concept } from './concept.model';
import { ConceptService } from './concept.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-concept',
    templateUrl: './concept.component.html'
})
export class ConceptComponent implements OnInit, OnDestroy {
concepts: Concept[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private conceptService: ConceptService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
        this.jhiLanguageService.setLocations(['concept']);
    }

    loadAll() {
        this.conceptService.query().subscribe(
            (res: Response) => {
                this.concepts = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConcepts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: Concept) {
        return item.id;
    }

    indexAll() {
        this.conceptService.indexAll().subscribe(
            (res: Response) => this.alertService.success('Indexed successfully', null, null),
            (res: Response) => this.alertService.error(res.json().message, null, null)
        );
    }

    registerChangeInConcepts() {
        this.eventSubscriber = this.eventManager.subscribe('conceptListModification', (response) => this.loadAll());
    }


    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
