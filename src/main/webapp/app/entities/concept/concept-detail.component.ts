import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Concept } from './concept.model';
import { ConceptService } from './concept.service';

@Component({
    selector: 'jhi-concept-detail',
    templateUrl: './concept-detail.component.html'
})
export class ConceptDetailComponent implements OnInit, OnDestroy {

    concept: Concept;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private conceptService: ConceptService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['concept']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.conceptService.find(id).subscribe(concept => {
            this.concept = concept;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
