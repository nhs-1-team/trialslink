import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { ConceptComponent } from './concept.component';
import { ConceptDetailComponent } from './concept-detail.component';
import { ConceptPopupComponent } from './concept-dialog.component';
import { ConceptDeletePopupComponent } from './concept-delete-dialog.component';

import { Principal } from '../../shared';


export const conceptRoute: Routes = [
  {
    path: 'concept',
    component: ConceptComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.concept.home.title'
    }
  }, {
    path: 'concept/:id',
    component: ConceptDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.concept.home.title'
    }
  }
];

export const conceptPopupRoute: Routes = [
  {
    path: 'concept-new',
    component: ConceptPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.concept.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'concept/:id/edit',
    component: ConceptPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.concept.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'concept/:id/delete',
    component: ConceptDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.concept.home.title'
    },
    outlet: 'popup'
  }
];
