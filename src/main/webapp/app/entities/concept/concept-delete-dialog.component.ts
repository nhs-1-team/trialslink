import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Concept } from './concept.model';
import { ConceptPopupService } from './concept-popup.service';
import { ConceptService } from './concept.service';

@Component({
    selector: 'jhi-concept-delete-dialog',
    templateUrl: './concept-delete-dialog.component.html'
})
export class ConceptDeleteDialogComponent {

    concept: Concept;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private conceptService: ConceptService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['concept']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.conceptService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'conceptListModification',
                content: 'Deleted an concept'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-concept-delete-popup',
    template: ''
})
export class ConceptDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private conceptPopupService: ConceptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.conceptPopupService
                .open(ConceptDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
