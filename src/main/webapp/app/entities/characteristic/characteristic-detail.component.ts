import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Characteristic } from './characteristic.model';
import { CharacteristicService } from './characteristic.service';

@Component({
    selector: 'jhi-characteristic-detail',
    templateUrl: './characteristic-detail.component.html'
})
export class CharacteristicDetailComponent implements OnInit, OnDestroy {

    characteristic: Characteristic;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private characteristicService: CharacteristicService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['characteristic', 'sex']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.characteristicService.find(id).subscribe(characteristic => {
            this.characteristic = characteristic;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
