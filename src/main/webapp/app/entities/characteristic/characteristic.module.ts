import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';

import {
    CharacteristicService,
    CharacteristicPopupService,
    CharacteristicComponent,
    CharacteristicDetailComponent,
    CharacteristicDialogComponent,
    CharacteristicPopupComponent,
    CharacteristicDeletePopupComponent,
    CharacteristicDeleteDialogComponent,
    characteristicRoute,
    characteristicPopupRoute,
} from './';

let ENTITY_STATES = [
    ...characteristicRoute,
    ...characteristicPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CharacteristicComponent,
        CharacteristicDetailComponent,
        CharacteristicDialogComponent,
        CharacteristicDeleteDialogComponent,
        CharacteristicPopupComponent,
        CharacteristicDeletePopupComponent,
    ],
    entryComponents: [
        CharacteristicComponent,
        CharacteristicDialogComponent,
        CharacteristicPopupComponent,
        CharacteristicDeleteDialogComponent,
        CharacteristicDeletePopupComponent,
    ],
    providers: [
        CharacteristicService,
        CharacteristicPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkCharacteristicModule {}
