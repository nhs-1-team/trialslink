import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { CharacteristicComponent } from './characteristic.component';
import { CharacteristicDetailComponent } from './characteristic-detail.component';
import { CharacteristicPopupComponent } from './characteristic-dialog.component';
import { CharacteristicDeletePopupComponent } from './characteristic-delete-dialog.component';

import { Principal } from '../../shared';


export const characteristicRoute: Routes = [
  {
    path: 'characteristic',
    component: CharacteristicComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
    }
  }, {
    path: 'characteristic/:id',
    component: CharacteristicDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
    }
  }
];

export const characteristicPopupRoute: Routes = [
  {
    path: 'characteristic-new/:trialId/create',
    component: CharacteristicPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'characteristic/:id/edit',
    component: CharacteristicPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'characteristic/:id/delete',
    component: CharacteristicDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
    },
    outlet: 'popup'
  }
];
