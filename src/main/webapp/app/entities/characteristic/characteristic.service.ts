import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Characteristic } from './characteristic.model';
@Injectable()
export class CharacteristicService {

    private resourceUrl = 'api/characteristics';
    private trialsResourceUrl = 'api/trials';

    constructor(private http: Http) { }

    create(characteristic: Characteristic): Observable<Characteristic> {
        let copy: Characteristic = Object.assign({}, characteristic);
        if (characteristic.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${characteristic.trialId}/criteria`, copy).map((res: Response) => {
                return res.json();
            });
        } else {
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                return res.json();
            });
        }
    }

    update(characteristic: Characteristic): Observable<Characteristic> {
        let copy: Characteristic = Object.assign({}, characteristic);
        return this.http.put(`${this.trialsResourceUrl}/${characteristic.trialId}/criteria`, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Characteristic> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        ;
    }

    criteriaForTrial(id: string, req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/criteria`, options);
    }

    delete(trialId: string, id: number): Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${trialId}/criteria/${id}`);
    }

    deleteAllForTrial(id: string): Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${id}/criteria`);
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        let options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            let params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            params.set('query', req.query);

            options.search = params;
        }
        return options;
    }
}
