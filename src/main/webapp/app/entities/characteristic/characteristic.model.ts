import { Age } from '../age/age.model';

const enum Sex {
    'MALE',
    'FEMALE',
    'ALL'

};
export class Characteristic {
    constructor(
        public id?: string,
        public criteria?: string,
        public code?: string,
        public trialId?: string,
        public sex?: Sex,
        public basedOnGender?: boolean,
        public acceptHealthyVolunteers?: boolean,
        public minAge?: Age,
        public maxAge?: Age
    ) {
        this.basedOnGender = false;
        this.acceptHealthyVolunteers = false;
        this.minAge = new Age();
        this.maxAge = new Age();
    }
}
