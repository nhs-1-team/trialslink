import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Characteristic } from './characteristic.model';
import { CharacteristicService } from './characteristic.service';
@Injectable()
export class CharacteristicPopupService {
    private isOpen = false;
    constructor (
        private modalService: NgbModal,
        private router: Router,
        private characteristicService: CharacteristicService

    ) {}

    open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (!createNew) {
            this.characteristicService.find(id).subscribe(characteristic => {
                this.characteristicModalRef(component, characteristic);
            });
        } else {
            let characteristic = new Characteristic();
            characteristic.trialId = id;
            return this.characteristicModalRef(component, characteristic);
        }
    }

    characteristicModalRef(component: Component, characteristic: Characteristic): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.characteristic = characteristic;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
