import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicPopupService } from './characteristic-popup.service';
import { CharacteristicService } from './characteristic.service';

@Component({
    selector: 'jhi-characteristic-delete-dialog',
    templateUrl: './characteristic-delete-dialog.component.html'
})
export class CharacteristicDeleteDialogComponent {

    characteristic: Characteristic;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private characteristicService: CharacteristicService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['characteristic', 'sex']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.characteristicService.delete(this.characteristic.trialId, id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'characteristicListModification',
                content: 'Deleted an characteristic'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-characteristic-delete-popup',
    template: ''
})
export class CharacteristicDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private characteristicPopupService: CharacteristicPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.characteristicPopupService
                .open(CharacteristicDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
