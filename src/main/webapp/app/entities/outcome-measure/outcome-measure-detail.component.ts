import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { OutcomeMeasure } from './outcome-measure.model';
import { OutcomeMeasureService } from './outcome-measure.service';

@Component({
    selector: 'jhi-outcome-measure-detail',
    templateUrl: './outcome-measure-detail.component.html'
})
export class OutcomeMeasureDetailComponent implements OnInit, OnDestroy {

    outcomeMeasure: OutcomeMeasure;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private outcomeMeasureService: OutcomeMeasureService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['outcomeMeasure']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.outcomeMeasureService.find(id).subscribe(outcomeMeasure => {
            this.outcomeMeasure = outcomeMeasure;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
