import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { OutcomeMeasure } from './outcome-measure.model';
import { OutcomeMeasurePopupService } from './outcome-measure-popup.service';
import { OutcomeMeasureService } from './outcome-measure.service';
@Component({
    selector: 'jhi-outcome-measure-dialog',
    templateUrl: './outcome-measure-dialog.component.html'
})
export class OutcomeMeasureDialogComponent implements OnInit {

    outcomeMeasure: OutcomeMeasure;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private outcomeMeasureService: OutcomeMeasureService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['outcomeMeasure']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.outcomeMeasure.id !== undefined) {
            this.outcomeMeasureService.update(this.outcomeMeasure)
                .subscribe((res: OutcomeMeasure) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.outcomeMeasureService.create(this.outcomeMeasure)
                .subscribe((res: OutcomeMeasure) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: OutcomeMeasure) {
        this.eventManager.broadcast({ name: 'outcomeMeasureListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-outcome-measure-popup',
    template: ''
})
export class OutcomeMeasurePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private outcomeMeasurePopupService: OutcomeMeasurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.outcomeMeasurePopupService
                    .open(OutcomeMeasureDialogComponent, params['id']);
            } else {
                this.modalRef = this.outcomeMeasurePopupService
                    .open(OutcomeMeasureDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
