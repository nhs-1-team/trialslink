import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OutcomeMeasure } from './outcome-measure.model';
import { OutcomeMeasureService } from './outcome-measure.service';
@Injectable()
export class OutcomeMeasurePopupService {
    private isOpen = false;
    constructor (
        private modalService: NgbModal,
        private router: Router,
        private outcomeMeasureService: OutcomeMeasureService

    ) {}

    open (component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.outcomeMeasureService.find(id).subscribe(outcomeMeasure => {
                this.outcomeMeasureModalRef(component, outcomeMeasure);
            });
        } else {
            return this.outcomeMeasureModalRef(component, new OutcomeMeasure());
        }
    }

    outcomeMeasureModalRef(component: Component, outcomeMeasure: OutcomeMeasure): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.outcomeMeasure = outcomeMeasure;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
