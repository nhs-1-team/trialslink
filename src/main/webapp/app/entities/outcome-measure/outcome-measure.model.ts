export class OutcomeMeasure {
    constructor(
        public id?: string,
        public title?: string,
        public description?: string,
        public timeFrame?: string,
    ) {
    }
}
