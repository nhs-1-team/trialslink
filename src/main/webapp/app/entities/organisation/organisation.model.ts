import { Address } from '../address/address.model';

export class Organisation {
    constructor(
        public id?: string,
        public name?: string,
        public address?: Address
    ) {
    }
}
