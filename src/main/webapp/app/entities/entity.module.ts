import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TrialslinkContactModule } from './contact/contact.module';
import { TrialslinkAddressModule } from './address/address.module';
import { TrialslinkAgeModule } from './age/age.module';
import { TrialslinkCharacteristicModule } from './characteristic/characteristic.module';
import { TrialslinkConceptModule } from './concept/concept.module';
import { TrialslinkInterventionModule } from './intervention/intervention.module';
import { TrialslinkIdInfoModule } from './id-info/id-info.module';
import { TrialslinkOrganisationModule } from './organisation/organisation.module';
import { TrialslinkOutcomeMeasureModule } from './outcome-measure/outcome-measure.module';
import { TrialslinkPractitionerModule } from './practitioner/practitioner.module';
import { TrialslinkRoleModule } from './role/role.module';
import { TrialslinkSecondaryIdModule } from './secondary-id/secondary-id.module';
import { TrialslinkTrialModule } from './trial/trial.module';
import { TrialslinkTrialCentreModule } from './trial-centre/trial-centre.module';
import { TrialslinkConditionModule } from './condition/condition.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TrialslinkContactModule,
        TrialslinkAddressModule,
        TrialslinkAgeModule,
        TrialslinkCharacteristicModule,
        TrialslinkConceptModule,
        TrialslinkInterventionModule,
        TrialslinkIdInfoModule,
        TrialslinkOrganisationModule,
        TrialslinkOutcomeMeasureModule,
        TrialslinkPractitionerModule,
        TrialslinkRoleModule,
        TrialslinkSecondaryIdModule,
        TrialslinkTrialModule,
        TrialslinkTrialCentreModule,
        TrialslinkConditionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkEntityModule {}
