import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Practitioner } from './practitioner.model';
import { PractitionerPopupService } from './practitioner-popup.service';
import { PractitionerService } from './practitioner.service';

@Component({
    selector: 'jhi-practitioner-delete-dialog',
    templateUrl: './practitioner-delete-dialog.component.html'
})
export class PractitionerDeleteDialogComponent {

    practitioner: Practitioner;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private practitionerService: PractitionerService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['practitioner', 'gender']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.practitionerService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'practitionerListModification',
                content: 'Deleted an practitioner'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-practitioner-delete-popup',
    template: ''
})
export class PractitionerDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private practitionerPopupService: PractitionerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.practitionerPopupService
                .open(PractitionerDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
