
const enum Gender {
    'MALE',
    ' FEMALE',
    ' OTHER',
    ' UNKNOWN'

};
export class Practitioner {
    constructor(
        public id?: string,
        public name?: string,
        public gender?: Gender,
    ) {
    }
}
