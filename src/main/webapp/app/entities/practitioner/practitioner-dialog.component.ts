import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Practitioner } from './practitioner.model';
import { PractitionerPopupService } from './practitioner-popup.service';
import { PractitionerService } from './practitioner.service';
@Component({
    selector: 'jhi-practitioner-dialog',
    templateUrl: './practitioner-dialog.component.html'
})
export class PractitionerDialogComponent implements OnInit {

    practitioner: Practitioner;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private practitionerService: PractitionerService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['practitioner', 'gender']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.practitioner.id !== undefined) {
            this.practitionerService.update(this.practitioner)
                .subscribe((res: Practitioner) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.practitionerService.create(this.practitioner)
                .subscribe((res: Practitioner) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Practitioner) {
        this.eventManager.broadcast({ name: 'practitionerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-practitioner-popup',
    template: ''
})
export class PractitionerPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private practitionerPopupService: PractitionerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.practitionerPopupService
                    .open(PractitionerDialogComponent, params['id']);
            } else {
                this.modalRef = this.practitionerPopupService
                    .open(PractitionerDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
