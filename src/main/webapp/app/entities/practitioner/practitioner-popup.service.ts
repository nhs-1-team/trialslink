import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Practitioner } from './practitioner.model';
import { PractitionerService } from './practitioner.service';
@Injectable()
export class PractitionerPopupService {
    private isOpen = false;
    constructor (
        private modalService: NgbModal,
        private router: Router,
        private practitionerService: PractitionerService

    ) {}

    open (component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.practitionerService.find(id).subscribe(practitioner => {
                this.practitionerModalRef(component, practitioner);
            });
        } else {
            return this.practitionerModalRef(component, new Practitioner());
        }
    }

    practitionerModalRef(component: Component, practitioner: Practitioner): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.practitioner = practitioner;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
