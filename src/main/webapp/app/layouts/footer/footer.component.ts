import { Component, OnInit } from '@angular/core';
import { InfoService} from '../../shared';

@Component({
    selector: 'jhi-footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

    appInfo: any;

    constructor(
        private infoService: InfoService
    ) {
        this.appInfo = {};
    }

    ngOnInit() {
        this.infoService.get().subscribe(appInfo => {
            this.appInfo.name = appInfo.name;
            this.appInfo.version = appInfo.version;
            this.appInfo.buildNumber = appInfo.buildNumber;
        });
    }
}
