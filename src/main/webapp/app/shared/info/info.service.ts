import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class InfoService  {
    constructor(private http: Http) { }

    get(): Observable<any> {
        return this.http.get('api/info').map((res: Response) => res.json());
    }
}
