import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgJhipsterModule } from 'ng-jhipster';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
// import { MasonryModule } from 'angular2-masonry';
import {SelectModule} from 'ng2-select';
// import {InlineEditorModule} from 'ng2-inline-editor';
import { TagInputModule } from 'ng2-tag-input';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // not needed for Angular 2.x version of TagInputModule

@NgModule({
    imports: [
        NgbModule.forRoot(),
        NgJhipsterModule.forRoot({
            i18nEnabled: true,
            defaultI18nLang: 'en'
        }),
        InfiniteScrollModule,
        SelectModule,
        TagInputModule
        // InlineEditorModule
    ],
    exports: [
        FormsModule,
        HttpModule,
        CommonModule,
        NgbModule,
        NgJhipsterModule,
        InfiniteScrollModule,
        // MasonryModule,
        SelectModule,
        TagInputModule
        // InlineEditorModule
    ]
})
export class TrialslinkSharedLibsModule {}
