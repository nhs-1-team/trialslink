package com.trialslink.repository;

import com.trialslink.domain.Organisation;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Organisation entity.
 */
@SuppressWarnings("unused")
public interface OrganisationRepository extends MongoRepository<Organisation,String> {
    Organisation findOneByName(String name);
}
