package com.trialslink.repository;

import com.trialslink.domain.TrialCentre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TrialCentre entity.
 */
@SuppressWarnings("unused")
public interface TrialCentreRepository extends MongoRepository<TrialCentre,String> {
    Page<TrialCentre> findByTrialId(String trialId, Pageable pageable);
}
