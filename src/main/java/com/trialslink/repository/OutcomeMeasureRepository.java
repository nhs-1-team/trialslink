package com.trialslink.repository;

import com.trialslink.domain.OutcomeMeasure;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the OutcomeMeasure entity.
 */
@SuppressWarnings("unused")
public interface OutcomeMeasureRepository extends MongoRepository<OutcomeMeasure,String> {

}
