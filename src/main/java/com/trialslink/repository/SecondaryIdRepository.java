package com.trialslink.repository;

import com.trialslink.domain.SecondaryId;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the SecondaryId entity.
 */
@SuppressWarnings("unused")
public interface SecondaryIdRepository extends MongoRepository<SecondaryId,String> {

}
