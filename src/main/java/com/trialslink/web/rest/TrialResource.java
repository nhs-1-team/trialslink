package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Trial;
import com.trialslink.service.TrialImporterService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import com.trialslink.web.rest.util.QueryModel;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.facet.result.Term;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing Trial.
 */
@RestController
@RequestMapping("/api")
public class TrialResource {

    private static final String ENTITY_NAME = "trial";
    private final Logger log = LoggerFactory.getLogger(TrialResource.class);
    private final TrialService trialService;
    private final TrialImporterService trialImporterService;

    public TrialResource(TrialService trialService,
                         TrialImporterService trialImporterService) {
        this.trialService = trialService;
        this.trialImporterService = trialImporterService;
    }

    /**
     * POST  /trials : Create a new trial.
     *
     * @param trial the trial to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trials")
    @Timed
    public ResponseEntity<Trial> createTrial(@Valid @RequestBody Trial trial) throws URISyntaxException {
        log.debug("REST request to save Trial : {}", trial);
        if (trial.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new trial cannot already have an ID")).body(null);
        }
        Trial result = trialService.save(trial);
        return ResponseEntity.created(new URI("/api/trials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /trials : Updates an existing trial.
     *
     * @param trial the trial to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trial,
     * or with status 400 (Bad Request) if the trial is not valid,
     * or with status 500 (Internal Server Error) if the trial couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trials")
    @Timed
    public ResponseEntity<Trial> updateTrial(@Valid @RequestBody Trial trial) throws URISyntaxException {
        log.debug("REST request to update Trial : {}", trial);
        if (trial.getId() == null) {
            return createTrial(trial);
        }
        Trial result = trialService.save(trial);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trial.getId()))
            .body(result);
    }

    /**
     * GET  /trials : get all the trials.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of trials in body
     */
    @GetMapping("/trials")
    @Timed
    public ResponseEntity<Map<String, Object>> getAllTrials(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Trials");
//        Page<Trial> page = trialService.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trials");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

        FacetedPage<Trial> page = trialService.findAllWithCategories(pageable);
        Set<String> items = new HashSet<>();
        items.add("type");
        items.add("status");
        items.add("phase");
        items.add("sites");
        items.add("sex");
        items.add("cstatus");
        Map<String, Set<Map<String, Object>>> facetsMap = new HashMap<>();
        for (String key : items) {
            log.info("Processed facet key : {}", key);
            // get search categories via facets
            Set<Map<String, Object>> mapSet = new HashSet<>();
            TermResult pageFacet = (TermResult) page.getFacet(key);
//            Aggregation pageFacet = page.getAggregation(key);

            if (pageFacet != null) {
                for (Term bucket : pageFacet.getTerms()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("type", bucket.getTerm());
                    map.put("count", bucket.getCount());
                    mapSet.add(map);
                }
            }

            // add to facets map
            facetsMap.put(key, mapSet);
        }
        log.debug("facetsMap {}", facetsMap);
        Map<String, Object> resultsMap = new HashMap<>();
        resultsMap.put("results", page.getContent());
        resultsMap.put("categories", facetsMap);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trials");
        return new ResponseEntity<>(resultsMap, headers, HttpStatus.OK);
    }

    /**
     * GET  /trials/:id : get the "id" trial.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trial, or with status 404 (Not Found)
     */
    @GetMapping("/trials/{id}")
    @Timed
    public ResponseEntity<Trial> getTrial(@PathVariable String id) {
        log.debug("REST request to get Trial : {}", id);
        Trial trial = trialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trial));
    }

    /**
     * GET  /trials/shortName/:shortName : get the "shortName" trial.
     *
     * @param shortName the shortName of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and boolean value of present or absent
     */
    @GetMapping("/trials/shortName/{shortName}")
    @Timed
    public ResponseEntity<Boolean> getTrialByShortName(@PathVariable String shortName) {
        log.debug("REST request to get Trial : {}", shortName);
        boolean result = false;
        Trial trial = trialService.findOneByShortName(shortName);
        if(trial != null){
            result = true;
        }
        return ResponseEntity.ok(result);
    }

    /**
     * DELETE  /trials/:id : delete the "id" trial.
     *
     * @param id the id of the trial to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trials/{id}")
    @Timed
    public ResponseEntity<Void> deleteTrial(@PathVariable String id) {
        log.debug("REST request to delete Trial : {}", id);
        trialService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * GET  /trials/:id/import : get the "id" Clinical Trails gov trial.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trial, or with status 404 (Not Found)
     */
    @GetMapping("/trials/{id}/import")
    @Timed
    public ResponseEntity<Trial> getFromClinicalTrialsGov(@PathVariable String id) throws IOException {
        log.debug("REST request to get Trial  from Clinical Trials Gov: {}", id);
        Trial trial = trialImporterService.importTrialFromClinicalTrailsGov(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trial));
    }

    /**
     * SEARCH  /_search/trials?query=:query : search for the trial corresponding
     * to the query.
     *
     * @param query the query of the trial search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @PostMapping("/_search/trials")
    @Timed
    public ResponseEntity<Map<String, Object>> searchTrials(@RequestBody QueryModel query, @ApiParam Pageable pageable)
            throws URISyntaxException {
        log.debug("REST request to search for a page of Trials for query {}", query);
        FacetedPage<Trial> page = trialService.search(query, pageable);
        Set<String> items = new HashSet<>();
        items.add("type");
        items.add("status");
        items.add("phase");
        items.add("sites");
        items.add("sex");
        items.add("cstatus");
        Map<String, Set<Map<String, Object>>> facetsMap = new HashMap<>();
        for (String key : items) {
            log.info("Processed facet key : {}", key);
            // get search categories via facets
            Set<Map<String, Object>> mapSet = new HashSet<>();
            TermResult pageFacet = (TermResult) page.getFacet(key);
//            Aggregation pageFacet = page.getAggregation(key);

            if (pageFacet != null) {
                for (Term bucket : pageFacet.getTerms()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("type", bucket.getTerm());
                    map.put("count", bucket.getCount());
                    mapSet.add(map);
                }
            }

            // add to facets map
            facetsMap.put(key, mapSet);
        }
        log.debug("facetsMap {}", facetsMap);
        Map<String, Object> resultsMap = new HashMap<>();
        resultsMap.put("results", page.getContent());
        resultsMap.put("categories", facetsMap);

        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query.getConditions().toString(), page, "/api/_search/trials");
        return new ResponseEntity<>(resultsMap, headers, HttpStatus.OK);
    }

    /**
     * SEARCH  /trials/omni_search?q=:query : search for the trial corresponding
     * to the query.
     *
     * @param query the query of the trial search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/trials/omni_search")
    @Timed
    public ResponseEntity<Map<String, Object>> omniSearchTrials(@RequestParam(name = "q") String query, @ApiParam Pageable pageable)
            throws URISyntaxException {
        log.debug("REST request to perform omni search for a page of Trials for query {}", query);
        FacetedPage<Trial> page = trialService.omniSearch(query, pageable);
        Set<String> items = new HashSet<>();
        items.add("type");
        items.add("status");
        items.add("phase");
        items.add("sites");
        items.add("sex");
        items.add("cstatus");
        Map<String, Set<Map<String, Object>>> facetsMap = new HashMap<>();
        for (String key : items) {
            log.info("Processed facet key : {}", key);
            // get search categories via facets
            Set<Map<String, Object>> mapSet = new HashSet<>();
            TermResult pageFacet = (TermResult) page.getFacet(key);
//            Aggregation pageFacet = page.getAggregation(key);

            if (pageFacet != null) {
                for (Term bucket : pageFacet.getTerms()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("type", bucket.getTerm());
                    map.put("count", bucket.getCount());
                    mapSet.add(map);
                }
            }

            // add to facets map
            facetsMap.put(key, mapSet);
        }
        log.debug("facetsMap {}", facetsMap);
        Map<String, Object> resultsMap = new HashMap<>();
        resultsMap.put("results", page.getContent());
        resultsMap.put("categories", facetsMap);

        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/trials/omni_search");
        return new ResponseEntity<>(resultsMap, headers, HttpStatus.OK);
    }

    /**
     * INDEX  /_index/trials : index all trials
     *
     * @return the result of the index action
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_index/trials")
    @Timed
    public ResponseEntity<Boolean> indexTrials() throws URISyntaxException {
        log.debug("REST request to bulk index all trials");
        Boolean result = trialService.indexAll();
        if (result) {
            return ResponseEntity.ok().body(true);
        }
        else {
            return ResponseEntity.badRequest().body(result);
        }
    }
}
