package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Age;

import com.trialslink.repository.AgeRepository;
import com.trialslink.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Age.
 */
@RestController
@RequestMapping("/api")
public class AgeResource {

    private final Logger log = LoggerFactory.getLogger(AgeResource.class);

    private static final String ENTITY_NAME = "age";
        
    private final AgeRepository ageRepository;

    public AgeResource(AgeRepository ageRepository) {
        this.ageRepository = ageRepository;
    }

    /**
     * POST  /ages : Create a new age.
     *
     * @param age the age to create
     * @return the ResponseEntity with status 201 (Created) and with body the new age, or with status 400 (Bad Request) if the age has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ages")
    @Timed
    public ResponseEntity<Age> createAge(@Valid @RequestBody Age age) throws URISyntaxException {
        log.debug("REST request to save Age : {}", age);
        if (age.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new age cannot already have an ID")).body(null);
        }
        Age result = ageRepository.save(age);
        return ResponseEntity.created(new URI("/api/ages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ages : Updates an existing age.
     *
     * @param age the age to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated age,
     * or with status 400 (Bad Request) if the age is not valid,
     * or with status 500 (Internal Server Error) if the age couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ages")
    @Timed
    public ResponseEntity<Age> updateAge(@Valid @RequestBody Age age) throws URISyntaxException {
        log.debug("REST request to update Age : {}", age);
        if (age.getId() == null) {
            return createAge(age);
        }
        Age result = ageRepository.save(age);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, age.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ages : get all the ages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ages in body
     */
    @GetMapping("/ages")
    @Timed
    public List<Age> getAllAges() {
        log.debug("REST request to get all Ages");
        List<Age> ages = ageRepository.findAll();
        return ages;
    }

    /**
     * GET  /ages/:id : get the "id" age.
     *
     * @param id the id of the age to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the age, or with status 404 (Not Found)
     */
    @GetMapping("/ages/{id}")
    @Timed
    public ResponseEntity<Age> getAge(@PathVariable String id) {
        log.debug("REST request to get Age : {}", id);
        Age age = ageRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(age));
    }

    /**
     * DELETE  /ages/:id : delete the "id" age.
     *
     * @param id the id of the age to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ages/{id}")
    @Timed
    public ResponseEntity<Void> deleteAge(@PathVariable String id) {
        log.debug("REST request to delete Age : {}", id);
        ageRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
