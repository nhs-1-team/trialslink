package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Concept;
import com.trialslink.service.ConceptService;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing Concept.
 */
@RestController
@RequestMapping("/api")
public class ConceptResource {

    private final Logger log = LoggerFactory.getLogger(ConceptResource.class);

    private static final String ENTITY_NAME = "concept";

    private final ConceptService conceptService;

    public ConceptResource(ConceptService conceptService) {
        this.conceptService = conceptService;
    }

    /**
     * POST  /concepts : Create a new concept.
     *
     * @param concept the concept to create
     * @return the ResponseEntity with status 201 (Created) and with body the new concept, or with status 400 (Bad Request) if the concept has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/concepts")
    @Timed
    public ResponseEntity<Concept> createConcept(@Valid @RequestBody Concept concept) throws URISyntaxException {
        log.debug("REST request to save Concept : {}", concept);
        if (concept.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new concept cannot already have an ID")).body(null);
        }
        Concept result = conceptService.save(concept);
        return ResponseEntity.created(new URI("/api/concepts/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /concepts : Updates an existing concept.
     *
     * @param concept the concept to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated concept,
     * or with status 400 (Bad Request) if the concept is not valid,
     * or with status 500 (Internal Server Error) if the concept couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/concepts")
    @Timed
    public ResponseEntity<Concept> updateConcept(@Valid @RequestBody Concept concept) throws URISyntaxException {
        log.debug("REST request to update Concept : {}", concept);
        if (concept.getId() == null) {
            return createConcept(concept);
        }
        Concept result = conceptService.save(concept);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, concept.getId()))
            .body(result);
    }

    /**
     * GET  /concepts : get all the concepts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of concepts in body
     */
    @GetMapping("/concepts")
    @Timed
    public ResponseEntity<List<Concept>> getAllConcepts(@ApiParam Pageable pageable) {
        log.debug("REST request to get all Concepts");
        Page<Concept> page = conceptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/concepts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /statuses : get all the statuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of statii in body
     */
    @GetMapping("/statuses")
    @Timed
    public ResponseEntity<List<Concept>> getAllStatusConcepts(@ApiParam Pageable pageable) {
        log.debug("REST request to get all Status Concepts");
        Page<Concept> page = conceptService.findAllByType(pageable, "status");
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/concepts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /concepts/:id : get the "id" concept.
     *
     * @param id the id of the concept to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the concept, or with status 404 (Not Found)
     */
    @GetMapping("/concepts/{id}")
    @Timed
    public ResponseEntity<Concept> getConcept(@PathVariable String id) {
        log.debug("REST request to get Concept : {}", id);
        Concept concept = conceptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(concept));
    }

    /**
     * DELETE  /concepts/:id : delete the "id" concept.
     *
     * @param id the id of the concept to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/concepts/{id}")
    @Timed
    public ResponseEntity<Void> deleteConcept(@PathVariable String id) {
        log.debug("REST request to delete Concept : {}", id);
        conceptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/concepts?query=:query : search for the concepts corresponding
     * to the query.
     *
     * @param query    the query of the concepts search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/concepts")
    @Timed
    public ResponseEntity<List<Concept>> searchConcepts(@RequestParam String query, @ApiParam Pageable pageable)
            throws URISyntaxException {
        log.debug("REST request to search for a page of concepts for query {}", query);
        QueryStringQueryBuilder queryBuilder = queryStringQuery(query);
        // default to using AND and name as field
        queryBuilder.defaultOperator(QueryStringQueryBuilder.Operator.AND);
        queryBuilder.defaultField("label");
        Page<Concept> page = conceptService.search(queryBuilder, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/concepts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * INDEX  /_index/concepts : index all concepts
     *
     * @return the result of the index action
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_index/concepts")
    @Timed
    public ResponseEntity<Boolean> indexConcepts() throws URISyntaxException {
        log.debug("REST request to bulk index all concepts");
        Boolean result = conceptService.indexAll();
        if (result) {
            return ResponseEntity.ok().body(true);
        }
        else {
            return ResponseEntity.badRequest().body(result);
        }
    }
}
