package com.trialslink.search;

import com.trialslink.domain.Intervention;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Intervention entity.
 */
public interface InterventionSearchRepository extends ElasticsearchRepository<Intervention, String> {
}
