package com.trialslink.search;

import com.trialslink.domain.Trial;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Trial entity.
 */
public interface TrialSearchRepository extends ElasticsearchRepository<Trial, String> {
}
