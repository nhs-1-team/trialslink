package com.trialslink.domain;

/**
 * An interface spec for visitable objects.
 */
public interface Visitable {

    void accept(Visitor visitor);
}
