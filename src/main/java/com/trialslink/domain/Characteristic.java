package com.trialslink.domain;

import com.trialslink.domain.enumeration.Sex;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Characteristic.
 */

@Document(collection = "characteristic")
public class Characteristic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("criteria")
    private String criteria;

    @Field("code")
    private String code;

    @NotNull
    @Field("sex")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private Sex sex;

    @Field("based_on_gender")
    private Boolean basedOnGender;

    @Field("accept_healthy_volunteers")
    private Boolean acceptHealthyVolunteers;

    @Field("min_age")
    private Age minAge;

    @Field("max_age")
    private Age maxAge;

    @Field("trial_id")
    private String trialId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCriteria() {
        return criteria;
    }

    public Characteristic criteria(String criteria) {
        this.criteria = criteria;
        return this;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getCode() {
        return code;
    }

    public Characteristic code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Sex getSex() {
        return sex;
    }

    public Characteristic sex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Boolean isBasedOnGender() {
        return basedOnGender;
    }

    public Characteristic basedOnGender(Boolean basedOnGender) {
        this.basedOnGender = basedOnGender;
        return this;
    }

    public void setBasedOnGender(Boolean basedOnGender) {
        this.basedOnGender = basedOnGender;
    }

    public Boolean isAcceptHealthyVolunteers() {
        return acceptHealthyVolunteers;
    }

    public Characteristic acceptHealthyVolunteers(Boolean acceptHealthyVolunteers) {
        this.acceptHealthyVolunteers = acceptHealthyVolunteers;
        return this;
    }

    public void setAcceptHealthyVolunteers(Boolean acceptHealthyVolunteers) {
        this.acceptHealthyVolunteers = acceptHealthyVolunteers;
    }

    public Age getMinAge() {
        return minAge;
    }

    public void setMinAge(Age minAge) {
        this.minAge = minAge;
    }

    public Age getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Age maxAge) {
        this.maxAge = maxAge;
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Characteristic characteristic = (Characteristic) o;
        if (characteristic.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, characteristic.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Characteristic{" +
            "id=" + id +
            ", criteria='" + criteria + "'" +
            ", code='" + code + "'" +
            ", sex='" + sex + "'" +
            ", basedOnGender='" + basedOnGender + "'" +
            ", acceptHealthyVolunteers='" + acceptHealthyVolunteers + "'" +
            '}';
    }
}
