package com.trialslink.domain.enumeration;

/**
 * The ContactType enumeration.
 */
public enum ContactType {
    EMAIL, PHONE, FAX
}
