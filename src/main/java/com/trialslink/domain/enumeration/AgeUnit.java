package com.trialslink.domain.enumeration;

/**
 * The AgeUnit enumeration.
 */
public enum AgeUnit {
    YEARS,MONTHS,WEEKS,DAYS,HOURS,MINUTES,NA
}
