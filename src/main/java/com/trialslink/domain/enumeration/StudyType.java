package com.trialslink.domain.enumeration;

/**
 * The StudyType enumeration.
 */
public enum StudyType {
    INTERVENTIONAL, OBSERVATIONAL, PATIENT_REGISTRY
}
