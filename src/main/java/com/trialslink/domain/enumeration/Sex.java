package com.trialslink.domain.enumeration;

/**
 * The Sex enumeration.
 */
public enum Sex {
    MALE,FEMALE,ALL
}
