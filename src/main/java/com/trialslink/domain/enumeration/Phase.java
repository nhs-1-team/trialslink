package com.trialslink.domain.enumeration;

/**
 * The Phase enumeration.
 */
public enum Phase {
    ONE, TWO, THREE, FOUR, ZERO, UNKNOWN
}
