package com.trialslink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Concept.
 */

@Document(collection = "concept")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "concept", type = "concept")
@Setting(settingPath = "/config/elasticsearch/settings/index-settings.json")
@Mapping(mappingPath = "/config/elasticsearch/mappings/concept-mappings.json")
public class Concept implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("label")
    private String label;

    @Field("system")
    private String system;

    @Field("code")
    private String code;

    @Field("type")
    private String type;

    @Field("children")
    private Set<String> children = new HashSet<>();

    @Field("parents")
    private Set<String> parents = new HashSet<>();

    public Concept(String label) {
        this.label = label;
    }

    public Concept() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Concept label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSystem() {
        return system;
    }

    public Concept system(String system) {
        this.system = system;
        return this;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCode() {
        return code;
    }

    public Concept code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public Concept type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getChildren() {
        return children;
    }

    public void setChildren(Set<String> children) {
        this.children = children;
    }

    public Set<String> getParents() {
        return parents;
    }

    public void setParents(Set<String> parents) {
        this.parents = parents;
    }

    public void addParent(Concept concept){
        this.parents.add(concept.getId());
        concept.getChildren().add(this.getId());
    }

    public void removeParent(Concept concept){
        this.parents.remove(concept.getId());
        concept.getChildren().remove(this.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Concept concept = (Concept) o;
        if (concept.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, concept.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Concept{" +
            "id=" + id +
            ", label='" + label + "'" +
            ", system='" + system + "'" +
            ", code='" + code + "'" +
                ", type='" + type + "'" +
                ", children=[" + children + "]" +
                ", parents=[" + parents + "]" +
            '}';
    }
}
