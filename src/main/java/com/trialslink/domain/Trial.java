package com.trialslink.domain;

import com.trialslink.config.CascadeSave;
import com.trialslink.domain.enumeration.Phase;
import com.trialslink.domain.enumeration.StudyType;
import com.trialslink.domain.enumeration.TrialStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Trial.
 */

@Document(collection = "clinical_trial")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "trial", type = "trial")
@Setting(settingPath = "/config/elasticsearch/settings/index-settings.json")
@Mapping(mappingPath = "/config/elasticsearch/mappings/trial-mappings.json")
public class Trial extends AbstractAuditingEntity implements Serializable, Visitable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String id;

    @NotNull
    @Field("title")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String, analyzer = "nameAnalyzer")
    private String title;

    @NotNull
    @Field("brief_title")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String, analyzer = "nameAnalyzer")
    private String briefTitle;

    @NotNull
    @Field("status")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private TrialStatus status;

    @NotNull
    @Field("type")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private StudyType type;

    @Field("phase")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private Set<Phase> phases = new HashSet<>();

    @Field("has_expanded_access")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Boolean)
    private Boolean hasExpandedAccess;

    @Field("category")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String category;

    @Field("focus")
    private String focus;

    @Field("keywords")
    private Set<String> keywords;

    @Field("description")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String description;

    @Field("record_verification_date")
    private ZonedDateTime recordVerificationDate;

    @Field("start_date")
    private ZonedDateTime startDate;

    @Field("primary_completion_date")
    private ZonedDateTime primaryCompletionDate;

    @Field("study_completion_date")
    private ZonedDateTime studyCompletionDate;

    @Field("reason_stopped")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String reasonStopped;

    @Field("note")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String note;

    @NotNull
    @Field("brief_summary")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String briefSummary;

    @Field("detailed_description")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.String)
    private String detailedDescription;

    @Field
    @CascadeSave
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<Characteristic> eligibilities = new HashSet<>();

    @Field
    @CascadeSave
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Organisation sponsor;

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Contact chiefInvestigator;

    @DBRef
    @CascadeSave
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<TrialCentre> sites = new HashSet<>();

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<Condition> conditions = new HashSet<>();

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<Intervention> interventions = new HashSet<>();

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<OutcomeMeasure> primaryOutcomes = new HashSet<>();

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<OutcomeMeasure> secondaryOutcomes = new HashSet<>();

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private Set<OutcomeMeasure> otherOutcomes = new HashSet<>();

    @Field
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    private IdInfo idInfo;

    @Field
    private String shortName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Trial title(String title) {
        this.title = title;
        return this;
    }

    public String getBriefTitle() {
        return briefTitle;
    }

    public void setBriefTitle(String briefTitle) {
        this.briefTitle = briefTitle;
    }

    public Trial briefTitle(String briefTitle) {
        this.briefTitle = briefTitle;
        return this;
    }

    public TrialStatus getStatus() {
        return status;
    }

    public void setStatus(TrialStatus status) {
        this.status = status;
    }

    public Trial status(TrialStatus status) {
        this.status = status;
        return this;
    }

    public StudyType getType() {
        return type;
    }

    public void setType(StudyType type) {
        this.type = type;
    }

    public Trial type(StudyType type) {
        this.type = type;
        return this;
    }

    public Set<Phase> getPhases() {
        return phases;
    }

    public void setPhases(Set<Phase> phases) {
        this.phases = phases;
    }

    public Trial phase(Set<Phase> phases) {
        this.phases = phases;
        return this;
    }

    public Boolean isHasExpandedAccess() {
        return hasExpandedAccess;
    }

    public Trial hasExpandedAccess(Boolean hasExpandedAccess) {
        this.hasExpandedAccess = hasExpandedAccess;
        return this;
    }

    public void setHasExpandedAccess(Boolean hasExpandedAccess) {
        this.hasExpandedAccess = hasExpandedAccess;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Trial category(String category) {
        this.category = category;
        return this;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public Trial focus(String focus) {
        this.focus = focus;
        return this;
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    public void addKeyword(String keyword) {
        keywords.add(keyword);
    }

    public void removeKeyword(String keyword) {
        keywords.remove(keyword);
    }

    public Trial keyword(Set<String> keywords) {
        this.keywords = keywords;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Trial description(String description) {
        this.description = description;
        return this;
    }

    public ZonedDateTime getRecordVerificationDate() {
        return recordVerificationDate;
    }

    public void setRecordVerificationDate(ZonedDateTime recordVerificationDate) {
        this.recordVerificationDate = recordVerificationDate;
    }

    public Trial recordVerificationDate(ZonedDateTime recordVerificationDate) {
        this.recordVerificationDate = recordVerificationDate;
        return this;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public Trial startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public ZonedDateTime getPrimaryCompletionDate() {
        return primaryCompletionDate;
    }

    public void setPrimaryCompletionDate(ZonedDateTime primaryCompletionDate) {
        this.primaryCompletionDate = primaryCompletionDate;
    }

    public Trial primaryCompletionDate(ZonedDateTime primaryCompletionDate) {
        this.primaryCompletionDate = primaryCompletionDate;
        return this;
    }

    public ZonedDateTime getStudyCompletionDate() {
        return studyCompletionDate;
    }

    public void setStudyCompletionDate(ZonedDateTime studyCompletionDate) {
        this.studyCompletionDate = studyCompletionDate;
    }

    public Trial studyCompletionDate(ZonedDateTime studyCompletionDate) {
        this.studyCompletionDate = studyCompletionDate;
        return this;
    }

    public String getReasonStopped() {
        return reasonStopped;
    }

    public void setReasonStopped(String reasonStopped) {
        this.reasonStopped = reasonStopped;
    }

    public Trial reasonStopped(String reasonStopped) {
        this.reasonStopped = reasonStopped;
        return this;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Trial note(String note) {
        this.note = note;
        return this;
    }

    public String getBriefSummary() {
        return briefSummary;
    }

    public void setBriefSummary(String briefSummary) {
        this.briefSummary = briefSummary;
    }

    public Trial briefSummary(String briefSummary) {
        this.briefSummary = briefSummary;
        return this;
    }

    public String getDetailedDescription() {
        return detailedDescription;
    }

    public void setDetailedDescription(String detailedDescription) {
        this.detailedDescription = detailedDescription;
    }

    public Trial detailedDescription(String detailedDescription) {
        this.detailedDescription = detailedDescription;
        return this;
    }

    public Set<Characteristic> getEligibilities() {
        return eligibilities;
    }

    public void setEligibilities(Set<Characteristic> characteristics) {
        this.eligibilities = characteristics;
    }

    public Trial eligibilities(Set<Characteristic> characteristics) {
        this.eligibilities = characteristics;
        return this;
    }

    public Trial addEligibility(Characteristic characteristic) {
        this.eligibilities.add(characteristic);
        return this;
    }

    public Trial removeEligibility(Characteristic characteristic) {
        this.eligibilities.remove(characteristic);
        return this;
    }

    public Organisation getSponsor() {
        return sponsor;
    }

    public void setSponsor(Organisation organisation) {
        this.sponsor = organisation;
    }

    public Trial sponsor(Organisation organisation) {
        this.sponsor = organisation;
        return this;
    }

    public Contact getChiefInvestigator() {
        return chiefInvestigator;
    }

    public void setChiefInvestigator(Contact practitionerRole) {
        this.chiefInvestigator = practitionerRole;
    }

    public Trial chiefInvestigator(Contact practitionerRole) {
        this.chiefInvestigator = practitionerRole;
        return this;
    }

    public Set<TrialCentre> getSites() {
        return sites;
    }

    public void setSites(Set<TrialCentre> trialCentres) {
        this.sites = trialCentres;
    }

    public Trial sites(Set<TrialCentre> trialCentres) {
        this.sites = trialCentres;
        return this;
    }

    public Trial addSite(TrialCentre trialCentre) {
        this.sites.add(trialCentre);
        trialCentre.setTrialId(getId());
        return this;
    }

    public Trial removeSite(TrialCentre trialCentre) {
        this.sites.remove(trialCentre);
        trialCentre.setTrialId(null);
        return this;
    }

    public Set<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(Set<Condition> concepts) {
        this.conditions = concepts;
    }

    public Trial conditions(Set<Condition> concepts) {
        this.conditions = concepts;
        return this;
    }

    public Trial addConditions(Condition concept) {
        this.conditions.add(concept);
        return this;
    }

    public Trial updateCondition(String conditionId, Condition updatedCondition) {
        Condition existing = null;
        for (Condition condition : getConditions()) {
            if (condition.getId().equalsIgnoreCase(conditionId)) {
                existing = condition;
                break;
            }
        }
        if (existing != null) {
            getConditions().remove(existing);
        }

        this.conditions.add(updatedCondition);
        return this;
    }

    public Trial updateEligibility(String eligibilityId, Characteristic updatedCharacteristic) {
        Characteristic existing = null;
        for (Characteristic characteristic : getEligibilities()) {
            if (characteristic.getId().equalsIgnoreCase(eligibilityId)) {
                existing = characteristic;
                break;
            }
        }
        if (existing != null) {
            getEligibilities().remove(existing);
        }

        this.eligibilities.add(updatedCharacteristic);
        return this;
    }

    public Trial removeConditions(Condition concept) {
        this.conditions.remove(concept);
        return this;
    }

    public Set<Intervention> getInterventions() {
        return interventions;
    }

    public void setInterventions(Set<Intervention> interventions) {
        this.interventions = interventions;
    }

    public Trial interventions(Set<Intervention> interventions) {
        this.interventions = interventions;
        return this;
    }

    public Trial addInterventions(Intervention intervention) {
        this.interventions.add(intervention);
        return this;
    }

    public Trial updateIntervention(String interventionId, Intervention updatedIntervention) {
        Intervention existing = null;
        for(Intervention intervention: getInterventions()) {
            if (intervention.getId().equalsIgnoreCase(interventionId)) {
                existing = intervention;
                break;
            }
        }
        if(existing != null) {
            getInterventions().remove(existing);
        }

        this.interventions.add(updatedIntervention);
        return this;
    }

    public Trial removeInterventions(Intervention intervention) {
        this.interventions.remove(intervention);
        return this;
    }

    public Set<OutcomeMeasure> getPrimaryOutcomes() {
        return primaryOutcomes;
    }

    public void setPrimaryOutcomes(Set<OutcomeMeasure> outcomeMeasures) {
        this.primaryOutcomes = outcomeMeasures;
    }

    public Trial primaryOutcomes(Set<OutcomeMeasure> outcomeMeasures) {
        this.primaryOutcomes = outcomeMeasures;
        return this;
    }

    public Trial addPrimaryOutcomes(OutcomeMeasure outcomeMeasure) {
        this.primaryOutcomes.add(outcomeMeasure);
        return this;
    }

    public Trial removePrimaryOutcomes(OutcomeMeasure outcomeMeasure) {
        this.primaryOutcomes.remove(outcomeMeasure);
        return this;
    }

    public Set<OutcomeMeasure> getSecondaryOutcomes() {
        return secondaryOutcomes;
    }

    public void setSecondaryOutcomes(Set<OutcomeMeasure> outcomeMeasures) {
        this.secondaryOutcomes = outcomeMeasures;
    }

    public Trial secondaryOutcomes(Set<OutcomeMeasure> outcomeMeasures) {
        this.secondaryOutcomes = outcomeMeasures;
        return this;
    }

    public Trial addSecondaryOutcomes(OutcomeMeasure outcomeMeasure) {
        this.secondaryOutcomes.add(outcomeMeasure);
        return this;
    }

    public Trial removeSecondaryOutcomes(OutcomeMeasure outcomeMeasure) {
        this.secondaryOutcomes.remove(outcomeMeasure);
        return this;
    }

    public Set<OutcomeMeasure> getOtherOutcomes() {
        return otherOutcomes;
    }

    public void setOtherOutcomes(Set<OutcomeMeasure> outcomeMeasures) {
        this.otherOutcomes = outcomeMeasures;
    }

    public Trial otherOutcomes(Set<OutcomeMeasure> outcomeMeasures) {
        this.otherOutcomes = outcomeMeasures;
        return this;
    }

    public Trial addOtherOutcomes(OutcomeMeasure outcomeMeasure) {
        this.otherOutcomes.add(outcomeMeasure);
        return this;
    }

    public Trial removeOtherOutcomes(OutcomeMeasure outcomeMeasure) {
        this.otherOutcomes.remove(outcomeMeasure);
        return this;
    }

    public IdInfo getIdInfo() {
        return idInfo;
    }

    public void setIdInfo(IdInfo idInfo) {
        this.idInfo = idInfo;
    }

    public Trial idInfo(IdInfo idInfo) {
        this.idInfo = idInfo;
        return this;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Trial trial = (Trial) o;
        if (trial.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, trial.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Trial{" +
            "id=" + id +
            ", title='" + title + "'" +
            ", briefTitle='" + briefTitle + "'" +
            ", status='" + status + "'" +
            ", type='" + type + "'" +
            ", phases='" + phases + "'" +
            ", hasExpandedAccess='" + hasExpandedAccess + "'" +
            ", shortName='" + shortName + "'" +
            ", focus='" + focus + "'" +
            ", keywords='" + keywords + "'" +
            ", description='" + description + "'" +
            ", recordVerificationDate='" + recordVerificationDate + "'" +
            ", startDate='" + startDate + "'" +
            ", primaryCompletionDate='" + primaryCompletionDate + "'" +
            ", studyCompletionDate='" + studyCompletionDate + "'" +
            ", reasonStopped='" + reasonStopped +
            '}';
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
