package com.trialslink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Condition.
 */

@Document(collection = "condition")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "condition", type = "condition")
@Setting(settingPath = "/config/elasticsearch/settings/index-settings.json")
@Mapping(mappingPath = "/config/elasticsearch/mappings/condition-mappings.json")
public class Condition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("label")
    private String label;

    @Field("system")
    private String system;

    @Field("code")
    private String code;

    @Field("status")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private String status;

    @Field("status_code")
    private String statusCode;

    @Field("trial_id")
    private String trialId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Condition label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSystem() {
        return system;
    }

    public Condition system(String system) {
        this.system = system;
        return this;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCode() {
        return code;
    }

    public Condition code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public Condition status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public Condition statusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Condition condition = (Condition) o;
        if (condition.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, condition.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Condition{" +
                "id=" + id +
                ", label='" + label + "'" +
                ", system='" + system + "'" +
                ", code='" + code + "'" +
                ", status='" + status + "'" +
                ", statusCode='" + statusCode + "'" +
                ", trialId='" + trialId + "'" +
                '}';
    }
}
