package com.trialslink.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A IdInfo.
 */

@Document(collection = "id_info")
@XStreamAlias(value = "id_info")
public class IdInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("protocol_id")
    @XStreamAlias("org_study_id")
    private String protocolId;

    @Field("nct_id")
    @XStreamAlias(value = "nct_id")
    private String nctId;

    @XStreamImplicit(itemFieldName = "secondary_id")
    private Set<String> secondaryIds = new HashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(String protocolId) {
        this.protocolId = protocolId;
    }

    public IdInfo protocolId(String protocolId) {
        this.protocolId = protocolId;
        return this;
    }

    public String getNctId() {
        return nctId;
    }

    public void setNctId(String nctId) {
        this.nctId = nctId;
    }

    public IdInfo nctId(String nctId) {
        this.nctId = nctId;
        return this;
    }

    public Set<String> getSecondaryIds() {
        return secondaryIds;
    }

    public void setSecondaryIds(Set<String> secondaryIds) {
        this.secondaryIds = secondaryIds;
    }

    public IdInfo secondaryIds(Set<String> secondaryIds) {
        this.secondaryIds = secondaryIds;
        return this;
    }

    public IdInfo addSecondaryId(String secondaryId) {
        this.secondaryIds.add(secondaryId);
        return this;
    }

    public IdInfo removeSecondaryId(String secondaryId) {
        this.secondaryIds.remove(secondaryId);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IdInfo idInfo = (IdInfo) o;
        if (idInfo.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, idInfo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "IdInfo{" +
            "id=" + id +
            ", protocolId='" + protocolId + "'" +
            ", nctId='" + nctId + "'" +
            '}';
    }
}
