package com.trialslink;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trialslink.config.ApplicationProperties;
import com.trialslink.config.DefaultProfileUtil;
import com.trialslink.domain.*;
import com.trialslink.repository.ConceptRepository;
import com.trialslink.repository.OrganisationRepository;
import com.trialslink.repository.RoleRepository;
import com.trialslink.repository.TrialRepository;
import com.trialslink.service.*;
import com.trialslink.service.util.LookupGenerator;
import com.vladsch.flexmark.ast.Document;
import com.vladsch.flexmark.ast.Node;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.options.MutableDataSet;
import io.github.jhipster.config.JHipsterConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.oxm.xstream.XStreamMarshaller;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@ComponentScan
@EnableAutoConfiguration(exclude = {MetricFilterAutoConfiguration.class, MetricRepositoryAutoConfiguration.class, MongoRepositoriesAutoConfiguration.class})
@EnableConfigurationProperties({ApplicationProperties.class})
public class TrialslinkApp {

    private static final Logger log = LoggerFactory.getLogger(TrialslinkApp.class);

    private final Environment env;

    public TrialslinkApp(Environment env) {
        this.env = env;
    }

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments
     * @throws UnknownHostException if the local host name could not be resolved into an address
     */
    public static void main(String[] args) throws UnknownHostException {
        SpringApplication app = new SpringApplication(TrialslinkApp.class);
        DefaultProfileUtil.addDefaultProfile(app);
        ConfigurableApplicationContext ctx = app.run(args);
        Environment env = ctx.getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' version '{}' build '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            env.getProperty("info.project.version"),
            env.getProperty("info.project.buildNumber"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"),
            env.getActiveProfiles());

        if (Arrays.asList(env.getActiveProfiles()).contains("dev") || Arrays.asList(env.getActiveProfiles()).contains("prod")) {
            // add sample data if none exists
            verifyAndImportCodes(ctx);
            verifyAndImportOrganisations(ctx);
            verifyAndImportRoles(ctx);
            verifyAndImportTrials(ctx);
            // index all entities
            indexEntities(ctx);
        }
    }

    /**
     * Utility bootstrap method that imports concepts if none is found.
     * @param ctx the application context
     */
    private static void verifyAndImportCodes(ConfigurableApplicationContext ctx) {

        ConceptService conceptService = ctx.getBean(ConceptService.class);
        long count = ctx.getBean(ConceptRepository.class).count();
        log.info("concepts.size() = " + count);

        if (count == 0) {

            MutableDataSet options = new MutableDataSet();;
            Parser parser = Parser.builder(options).build();

            try {

                Node document = parser.parseReader(new InputStreamReader(TrialslinkApp.class.getClassLoader()
                        .getResourceAsStream("config/conditions.md"), StandardCharsets.UTF_8));
                processNode(document, conceptService);
            } catch (IOException e) {
                log.error("Nested exception is : ", e);
            }

            Set<String> statusCodes = new HashSet<>();
            statusCodes.add("New / Untreated");
            statusCodes.add("Relapsed / Refractory");
            statusCodes.add("Relapsed > 1");
            statusCodes.add("Complete Remission (1st)");
            statusCodes.add("Complete Remission (>1)");
            statusCodes.add("Post Allograft (Remission)");
            statusCodes.add("Relapsed (>1)");

            for (String status : statusCodes) {
                Concept existing = conceptService.findOneByLabel(status);
                if (existing == null) {
                    Concept concept = new Concept().label(status).type("status");
                    conceptService.save(concept);
                }
            }

            // update lookups
            log.info("Updating lookups");
            LookupGenerator lookupGenerator = ctx.getBean(LookupGenerator.class);
            lookupGenerator.populateLookups();
        }
    }

    /**
     * Utility bootstrap method that imports organisations if none is found.
     * @param ctx the application context
     */
    private static void verifyAndImportOrganisations(ConfigurableApplicationContext ctx) {

        OrganisationService organisationService = ctx.getBean(OrganisationService.class);
        long count = ctx.getBean(OrganisationRepository.class).count();
        log.info("organisations.size() = " + count);

        if (count == 0) {

            try (InputStream inputStream = TrialslinkApp.class.getClassLoader().getResourceAsStream("config/organisations.csv")){
                BufferedReader bufReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                int counter = 0;
                String line = bufReader.readLine();
                while(line != null){
                    if(counter > 0){
                        Organisation organisation = new Organisation();
                        Address address = new Address();
                        String[] parts = line.split(",");
                        // create organisation form parts
                        address.setStreet(parts[2] + "," + parts[3]);
                        address.setCity(parts[5]);
                        address.setPostalCode(parts[7]);
                        address.setCountry("United Kingdom");
                        organisation.setName(parts[0]);
                        organisation.setAddress(address);

                        // save organisation
                        organisationService.save(organisation);
                    }
                    counter++;
                    line = bufReader.readLine();
                }
            } catch (IOException e){
                log.error("Unable to read init template from class path. Nested exception is : ", e);
            }
        }
    }

    /**
     * Utility bootstrap method that imports trials if none is found.
     * @param ctx the application context
     */
    private static void verifyAndImportTrials(ConfigurableApplicationContext ctx) {

        TrialService trialService = ctx.getBean(TrialService.class);
        TrialVisitorService trialVisitorService = ctx.getBean(TrialVisitorService.class);
        ObjectMapper objectMapper = ctx.getBean(ObjectMapper.class);
        long count = ctx.getBean(TrialRepository.class).count();
        log.info("Trials count = " + count);

        if (count == 0) {

            try (InputStream inputStream = TrialslinkApp.class.getClassLoader().getResourceAsStream("config/trials.json")){
                List trials = objectMapper.readValue(inputStream, List.class);
                trials.forEach(o -> {
                    if (o instanceof LinkedHashMap) {
                        Trial t = objectMapper.convertValue(o, Trial.class);
                        trialService.save(t);
                        // reattach all conditions
                        trialVisitorService.visit(t);
                    }
                });
                log.info("Imported {} trials ", ctx.getBean(TrialRepository.class).count());

            } catch (NullPointerException | IOException e){
                log.error("Unable to read trials data from class path. Nested exception is : ", e);
            }
        }
    }

    /**
     * Utility bootstrap method that imports roles if none is found.
     * @param ctx the application context
     */
    private static void verifyAndImportRoles(ConfigurableApplicationContext ctx) {

        RoleService roleService = ctx.getBean(RoleService.class);
        long count = ctx.getBean(RoleRepository.class).count();
        log.info("roles.size() = " + count);

        if (count == 0) {

            try (InputStream inputStream = TrialslinkApp.class.getClassLoader().getResourceAsStream("config/roles.csv")){
                BufferedReader bufReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                int counter = 0;
                String line = bufReader.readLine();
                while(line != null){
                    if(counter > 0){
                        Role role = new Role();
                        String[] parts = line.split(",");
                        // create role form parts
                        role.setName(parts[0]);
                        // save role
                        roleService.save(role);
                    }
                    counter++;
                    line = bufReader.readLine();
                }
            } catch (IOException e){
                log.error("Unable to read init template from class path. Nested exception is : ", e);
            }
        }
    }

    private static void processNode(Node childNode, ConceptService conceptService) {

        if("Text".equalsIgnoreCase(childNode.getNodeName())){
            // hardcoded way to access text of child and parent concept. The number of times getParent needs to be called
            // is very specific to how the AST looks for the Markdown in flexmark library
            log.debug("childNode.getChars().toString() = {}", childNode.getChars().toString());
            log.debug("childNode.getParent().getParent().getParent().getParent() = {}", childNode.getParent().getParent().getParent().getParent());
            log.debug("childNode.getParent().getParent().getParent().getParent().getFirstChild().getChars() = {}", childNode.getParent().getParent().getParent().getParent().getFirstChild().getChars());

            Concept parentConcept = null;
            if(!(childNode.getParent().getParent().getParent().getParent() instanceof Document)) {
                parentConcept = conceptService.findOneByLabel(childNode.getParent().getParent().getParent().getParent().getFirstChild().getChars().toString().trim());
            }
            Concept concept = new Concept(childNode.getChars().toString().trim()).type("condition");
            if(parentConcept != null) {
                concept.addParent(parentConcept);
            }
            concept = conceptService.save(concept);
            log.debug("concept = {}", concept);
            log.debug("parentConcept = {}", parentConcept);
        }

        for(Node child : childNode.getChildren()) {
            processNode(child, conceptService);
        }
    }

    /**
     * Utility bootstrap method that generates index for all entities
     * @param ctx the application context
     */
    private static void indexEntities(ConfigurableApplicationContext ctx) {

        log.info("Starting bulk index of entities. Please wait...");
        TrialService trialService = ctx.getBean(TrialService.class);
        ConceptService conceptService = ctx.getBean(ConceptService.class);
        ContactService contactService = ctx.getBean(ContactService.class);

        trialService.indexAll();
        conceptService.indexAll();
        contactService.indexAll();
        log.info("Finished bulk index of entities. Starting up...");
    }

    /**
     * Initializes trialslink.
     * <p>
     * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
     * <p>
     * You can find more information on how profiles work with JHipster on <a href="http://jhipster.github.io/profiles/">http://jhipster.github.io/profiles/</a>.
     */
    @PostConstruct
    public void initApplication() {
        Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
            log.error("You have misconfigured your application! It should not run " +
                "with both the 'dev' and 'prod' profiles at the same time.");
        }
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
            log.error("You have misconfigured your application! It should not" +
                "run with both the 'dev' and 'cloud' profiles at the same time.");
        }
    }

    @Bean
    public XStreamMarshaller getXStreamMarshaller() {
        XStreamMarshaller xstreamMarshaller = new XStreamMarshaller();
        xstreamMarshaller.setSupportedClasses(Trial.class);
        xstreamMarshaller.setAutodetectAnnotations(true);
        xstreamMarshaller.getXStream().ignoreUnknownElements();
        return xstreamMarshaller;
    }
}
