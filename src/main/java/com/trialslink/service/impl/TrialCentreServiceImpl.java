package com.trialslink.service.impl;

import com.trialslink.domain.Organisation;
import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialCentre;
import com.trialslink.repository.TrialCentreRepository;
import com.trialslink.service.OrganisationService;
import com.trialslink.service.TrialCentreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing TrialCentre.
 */
@Service
public class TrialCentreServiceImpl implements TrialCentreService{

    private final Logger log = LoggerFactory.getLogger(TrialCentreServiceImpl.class);
    
    private final TrialCentreRepository trialCentreRepository;
    private final OrganisationService organisationService;

    public TrialCentreServiceImpl(TrialCentreRepository trialCentreRepository,
                                  OrganisationService organisationService) {
        this.trialCentreRepository = trialCentreRepository;
        this.organisationService = organisationService;
    }

    /**
     * Save a trialCentre.
     *
     * @param trialCentre the entity to save
     * @return the persisted entity
     */
    @Override
    public TrialCentre save(TrialCentre trialCentre) {
        log.debug("Request to save TrialCentre : {}", trialCentre);
        // find any organisation with name - if we have one, we reuse it
        Organisation org = organisationService.findOneByName(trialCentre.getName());
        if(org == null){
            org = organisationService.save(trialCentre.asOrganisation());
        }
        // copy properties from org
        trialCentre.fromOrganisation(org);
        TrialCentre result = trialCentreRepository.save(trialCentre);
        return result;
    }

    /**
     *  Get all the trialCentres.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<TrialCentre> findAll(Pageable pageable) {
        log.debug("Request to get all TrialCentres");
        Page<TrialCentre> result = trialCentreRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get all the trialCentres for a given trial.
     *
     *  @param trial the trial
     *  @return the list of entities
     */
    @Override
    public Page<TrialCentre> findAllForTrial(Trial trial, Pageable pageable) {
        log.debug("Request to get all TrialCentres");
        Page<TrialCentre> result = trialCentreRepository.findByTrialId(trial.getId(), pageable);
        return result;
    }

    /**
     *  Get one trialCentre by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public TrialCentre findOne(String id) {
        log.debug("Request to get TrialCentre : {}", id);
        TrialCentre trialCentre = trialCentreRepository.findOne(id);
        return trialCentre;
    }

    /**
     *  Delete the  trialCentre by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete TrialCentre : {}", id);
        trialCentreRepository.delete(id);
    }

    /**
     * Search for the trialCentre corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TrialCentre> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TrialCentres for query {}", query);
        return null;
    }
}
