package com.trialslink.service.impl;

import com.trialslink.domain.Condition;
import com.trialslink.repository.ConditionRepository;
import com.trialslink.search.ConditionSearchRepository;
import com.trialslink.service.ConditionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link com.trialslink.domain.Condition}s.
 */
@Service
public class ConditionServiceImpl implements ConditionService {

    private final Logger log = LoggerFactory.getLogger(ConditionServiceImpl.class);

    private final ConditionRepository conditionRepository;
    private final ConditionSearchRepository conditionSearchRepository;

    public ConditionServiceImpl(ConditionRepository conditionRepository,
                                ConditionSearchRepository conditionSearchRepository) {
        this.conditionRepository = conditionRepository;
        this.conditionSearchRepository = conditionSearchRepository;
    }

    /**
     * Save a condition.
     *
     * @param condition the entity to save
     * @return the persisted entity
     */
    @Override
    public Condition save(Condition condition) {
        log.debug("Request to save Condition : {}", condition);
        Condition result = conditionRepository.save(condition);
        conditionSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the conditions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Condition> findAll(Pageable pageable) {
        log.debug("Request to get all Conditions");
        Page<Condition> result = conditionRepository.findAll(pageable);
        return result;
    }

    /**
     * Get all the conditions for trial.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Condition> findAllForTrial(String trialId, Pageable pageable) {
        log.debug("Request to get all Conditions");
        Page<Condition> result = conditionRepository.findByTrialId(trialId, pageable);
        return result;
    }

    /**
     * Get all the conditions as a list.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Condition> findAllAsList() {
        log.debug("Request to get all Conditions as a list");
        List<Condition> result = conditionRepository.findAll();
        return result;
    }

    /**
     * Get one condition by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Condition findOne(String id) {
        log.debug("Request to get Condition : {}", id);
        Condition condition = conditionRepository.findOne(id);
        return condition;
    }

    /**
     * Get one condition by label.
     *
     * @param label the label of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Condition findOneByLabel(String label) {
        log.debug("Request to get Condition by label : {}", label);
        Condition condition = conditionRepository.findOneByLabel(label);
        return condition;
    }

    /**
     * Delete the  Condition by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Condition : {}", id);
        conditionRepository.delete(id);
        conditionSearchRepository.delete(id);
    }

    /**
     * Delete all conditions
     */
    @Override
    public void deleteAll() {
        log.debug("Request to delete all Conditions");
        conditionRepository.deleteAll();
        conditionSearchRepository.deleteAll();
    }

    /**
     * Delete all conditions for trial
     */
    @Override
    public void deleteAllForTrial(String trialId) {
        log.debug("Request to delete all Conditions for trial {}", trialId);
        conditionRepository.deleteByTrialId(trialId);
        conditionSearchRepository.deleteByTrialId(trialId);
    }

    /**
     * Search for the conditions corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of matching entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Condition> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Condition for query {}", query);
        Page<Condition> result = conditionSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
