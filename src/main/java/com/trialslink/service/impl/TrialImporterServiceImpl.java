package com.trialslink.service.impl;

import com.trialslink.domain.*;
import com.trialslink.repository.*;
import com.trialslink.service.TrialCentreService;
import com.trialslink.service.TrialImporterService;
import com.trialslink.service.TrialService;
import com.trialslink.service.util.TrialXmlConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * A service implementation that allows trials to be imported from other sources
 */
@Service
public class TrialImporterServiceImpl implements TrialImporterService {

    private final Logger log = LoggerFactory.getLogger(TrialImporterServiceImpl.class);

    private final TrialService trialService;
    private final ContactRepository contactRepository;
    private final ConditionRepository conditionRepository;
    private final InterventionRepository interventionRepository;
    private final TrialCentreService trialCentreService;
    private final CharacteristicRepository characteristicRepository;
    private final XStreamMarshaller xStreamMarshaller;
    private final TrialXmlConverter trialXmlConverter = new TrialXmlConverter();

    public TrialImporterServiceImpl(TrialService trialService, XStreamMarshaller xStreamMarshaller,
                                    TrialCentreService trialCentreService,
                                    CharacteristicRepository characteristicRepository,
                                    ContactRepository contactRepository,
                                    ConditionRepository conditionRepository,
                                    InterventionRepository interventionRepository) {
        this.trialService = trialService;
        this.xStreamMarshaller = xStreamMarshaller;
        this.trialCentreService = trialCentreService;
        this.characteristicRepository = characteristicRepository;
        this.contactRepository = contactRepository;
        this.conditionRepository = conditionRepository;
        this.interventionRepository = interventionRepository;
    }

    @Override
    public Trial importTrialFromClinicalTrailsGov(String id) throws IOException {
        String url = "https://clinicaltrials.gov/show/" + id + "?displayxml=true";
        xStreamMarshaller.getXStream().alias("clinical_study", Trial.class);
        xStreamMarshaller.getXStream().registerConverter(trialXmlConverter);
        Trial trial = (Trial) xStreamMarshaller.unmarshalInputStream(new URL(url).openStream());
        log.debug("trial: {}", trial);
        // save characteristics
        Set<Characteristic> characteristics =  new HashSet<>();
        characteristics.addAll(trial.getEligibilities());
        trial.setEligibilities(new HashSet<>());
        characteristics.forEach(characteristic -> {
            trial.addEligibility(characteristicRepository.save(characteristic));
        });
        // collect and verify conditions and then finally add to trial
        Set<Condition> conditions = new HashSet<>();
        for (Condition condition : trial.getConditions()) {
            Condition existing = conditionRepository.findOneByLabel(condition.getLabel());
            if (existing == null) {
                existing = conditionRepository.save(condition);
            }
            conditions.add(existing);
        }
        trial.setConditions(conditions);

        // collect and verify interventions and then finally add to trial
        Set<Intervention> interventions = new HashSet<>();
        for (Intervention intervention : trial.getInterventions()) {
            Intervention existing = interventionRepository.findOneByLabel(intervention.getLabel());
            if (existing == null) {
                existing = interventionRepository.save(intervention);
            }
            interventions.add(existing);
        }
        trial.setInterventions(interventions);

        // we have to save sites explicitly - or db refs wont work
        Set<TrialCentre> sites = new HashSet<>();
        sites.addAll(trial.getSites());
        trial.setSites(new HashSet<>());
        Trial result = trialService.save(trial);
        sites.forEach(site -> {
            site.setTrialId(result.getId());
            TrialCentre centre = trialCentreService.save(site);
            centre.getContacts().forEach(contact -> {
                contact.setTrialCentreId(centre.getId());
                contactRepository.save(contact);
            });
            trial.addSite(centre);
        });
        log.debug("trial id info: {}", trial.getIdInfo());
        // now we need to update characteristics with trial id
        trial.getEligibilities().forEach(characteristic -> {
            characteristic.setTrialId(trial.getId());
            characteristicRepository.save(characteristic);
        });

        return trialService.save(trial);
    }
}

