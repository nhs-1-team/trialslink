package com.trialslink.service.impl;

import com.trialslink.domain.Concept;
import com.trialslink.repository.ConceptRepository;
import com.trialslink.search.ConceptSearchRepository;
import com.trialslink.service.ConceptService;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing {@link com.trialslink.domain.Concept}s.
 */
@Service
public class ConceptServiceImpl implements ConceptService {

    private final Logger log = LoggerFactory.getLogger(ConceptServiceImpl.class);

    private final ConceptRepository conceptRepository;
    private final ConceptSearchRepository conceptSearchRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;

    public ConceptServiceImpl(ConceptRepository conceptRepository,
                              ConceptSearchRepository conceptSearchRepository,
                              ElasticsearchTemplate elasticsearchTemplate) {
        this.conceptRepository = conceptRepository;
        this.conceptSearchRepository = conceptSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    /**
     * Save a Concept.
     *
     * @param concept the entity to save
     * @return the persisted entity
     */
    @Override
    public Concept save(Concept concept) {
        log.debug("Request to save Concept : {}", concept);
        // reuse existing id if org with same label exists
        Concept existing = findOneByLabel(concept.getLabel());
        if(existing != null){
            concept.setId(existing.getId());
        }
        Concept result = conceptRepository.save(concept);
        conceptSearchRepository.save(result);
        // get parents and save them if they are associated
        result.getParents().forEach(parentId -> {
            Concept parent = conceptRepository.findOne(parentId);
            parent.getChildren().add(result.getId());
            conceptRepository.save(parent);
            conceptSearchRepository.save(parent);
        });
        return result;
    }

    /**
     * Get all the Concepts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Concept> findAll(Pageable pageable) {
        log.debug("Request to get all Conditions");
        Page<Concept> result = conceptRepository.findAll(pageable);
        return result;
    }

    /**
     * Get all the Concepts as List
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Concept> findAllAsList() {
        log.debug("Request to get all Conditions");
        List<Concept> result = conceptRepository.findAll();
        return result;
    }

    /**
     * Get all the Concepts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Concept> findAllByType(Pageable pageable, String type) {
        log.debug("Request to get all Conditions");
        Page<Concept> result = conceptRepository.findAllByType(pageable, type);
        return result;
    }

    /**
     * Get one concept by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Concept findOne(String id) {
        log.debug("Request to get Condition : {}", id);
        Concept condition = conceptRepository.findOne(id);
        return condition;
    }

    /**
     * Get one concept by label.
     *
     * @param label the label of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Concept findOneByLabel(String label) {
        log.debug("Request to get Condition by label : {}", label);
        Concept condition = conceptRepository.findOneByLabel(label);
        return condition;
    }

    /**
     * Delete the  concept by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Condition : {}", id);
        // get parents and delete child associations
        Concept concept = conceptRepository.findOne(id);
        concept.getParents().forEach(parentId -> {
            Concept parent = conceptRepository.findOne(parentId);
            parent.getChildren().remove(concept.getId());
            conceptRepository.save(parent);
            conceptSearchRepository.save(parent);
        });
        conceptRepository.delete(id);
        conceptSearchRepository.delete(id);
    }

    /**
     * Delete all concepts
     */
    @Override
    public void deleteAll() {
        log.debug("Request to delete all concepts");
        conceptRepository.deleteAll();
        conceptSearchRepository.deleteAll();
    }

    /**
     * Search for the concepts corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of matching entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Concept> search(QueryBuilder query, Pageable pageable) {
        log.debug("Request to search for a page of Condition for query {}", query);
        Page<Concept> result = conceptSearchRepository.search(query, pageable);
        return result;
    }

    /**
     *  Index all the concepts.
     *
     *  @return the boolean that represents the success of the index action
     */
    @Override
    public boolean indexAll() {
        log.debug("Request to index all Concepts");
        boolean result = false;

        // delete existing indices
        try {
            elasticsearchTemplate.deleteIndex(Concept.class);
        } catch (IndexNotFoundException e) {
            log.error("Error deleting indices. Assuming index does not exist.");
        }

        try {
            conceptRepository.findAll().forEach(conceptSearchRepository::save);
            result = true;
        } catch (Exception e) {
            log.error("Error bulk indexing concepts. Nested exception is : ", e);
        }

        return result;
    }
}
