package com.trialslink.service;

import com.trialslink.domain.Organisation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Interface for managing Organisation.
 */
public interface OrganisationService {

    /**
     * Save a organisation.
     *
     * @param organisation the entity to save
     * @return the persisted entity
     */
    Organisation save(Organisation organisation);

    /**
     *  Get all the organisations.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Organisation> findAll(Pageable pageable);

    @Transactional(readOnly = true)
    List<Organisation> findAllAsList();

    /**
     *  Get the "id" organisation.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Organisation findOne(String id);

    Organisation findOneByName(String name);

    /**
     *  Delete the "id" organisation.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the organisation corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Organisation> search(String query, Pageable pageable);
}
