package com.trialslink.service;

import com.trialslink.domain.Trial;
import com.trialslink.web.rest.util.QueryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.FacetedPage;

/**
 * Service Interface for managing Trial.
 */
public interface TrialService {

    /**
     * Save a trial.
     *
     * @param trial the entity to save
     * @return the persisted entity
     */
    Trial save(Trial trial);

    /**
     *  Get all the trials.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Trial> findAll(Pageable pageable);

    /**
     *  Get the "id" trial.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Trial findOne(String id);

    Trial findOneByShortName(String shortName);

    /**
     *  Delete the "id" trial.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    FacetedPage<Trial> search(QueryModel query, Pageable pageable);

    /**
     * Index all the trials.
     *
     * @return the boolean that represents the success of the index action
     */
    boolean indexAll();

    /**
     * Omni search for the trial corresponding to the query but not on conditions or sites.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    FacetedPage<Trial> omniSearch(String query, Pageable pageable);

    FacetedPage<Trial> findAllWithCategories(Pageable pageable);
}
