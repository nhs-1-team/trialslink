package com.trialslink.service;

import com.trialslink.domain.Trial;

import java.io.IOException;

/**
 * A service implementation that allows trials to be imported from other sources
 */
public interface TrialImporterService {
    Trial importTrialFromClinicalTrailsGov(String id) throws IOException;
}
