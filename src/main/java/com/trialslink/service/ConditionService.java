package com.trialslink.service;

import com.trialslink.domain.Condition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * An interface specification for managing {@link Condition}s.
 */
public interface ConditionService {

    Condition save(Condition condition);

    Page<Condition> findAll(Pageable pageable);

    @Transactional(readOnly = true)
    Page<Condition> findAllForTrial(String trialId, Pageable pageable);

    List<Condition> findAllAsList();

    Condition findOne(String id);

    Condition findOneByLabel(String label);

    void delete(String id);

    void deleteAll();

    void deleteAllForTrial(String trialId);

    Page<Condition> search(String query, Pageable pageable);
}
