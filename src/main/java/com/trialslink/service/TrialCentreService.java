package com.trialslink.service;

import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialCentre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing TrialCentre.
 */
public interface TrialCentreService {

    /**
     * Save a trialCentre.
     *
     * @param trialCentre the entity to save
     * @return the persisted entity
     */
    TrialCentre save(TrialCentre trialCentre);

    /**
     *  Get all the trialCentres.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TrialCentre> findAll(Pageable pageable);

    Page<TrialCentre> findAllForTrial(Trial trial, Pageable pageable);

    /**
     *  Get the "id" trialCentre.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TrialCentre findOne(String id);

    /**
     *  Delete the "id" trialCentre.
     *
     *  @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the trialCentre corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TrialCentre> search(String query, Pageable pageable);
}
