package com.trialslink.service;

import com.trialslink.domain.Concept;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * An interface specification for managing {@link Concept}s.
 */
public interface ConceptService {

    Concept save(Concept concept);

    Page<Concept> findAll(Pageable pageable);

    List<Concept> findAllAsList();

    Page<Concept> findAllByType(Pageable pageable, String type);

    Concept findOne(String id);

    Concept findOneByLabel(String label);

    void delete(String id);

    void deleteAll();

    Page<Concept> search(QueryBuilder query, Pageable pageable);

    boolean indexAll();
}
