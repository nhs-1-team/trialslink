import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CharacteristicDetailComponent } from '../../../../../../main/webapp/app/entities/characteristic/characteristic-detail.component';
import { CharacteristicService } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.service';
import { Characteristic } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.model';

describe('Component Tests', () => {

    describe('Characteristic Management Detail Component', () => {
        let comp: CharacteristicDetailComponent;
        let fixture: ComponentFixture<CharacteristicDetailComponent>;
        let service: CharacteristicService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [CharacteristicDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    CharacteristicService
                ]
            }).overrideComponent(CharacteristicDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CharacteristicDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharacteristicService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Characteristic('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.characteristic).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
