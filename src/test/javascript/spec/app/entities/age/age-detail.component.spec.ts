import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AgeDetailComponent } from '../../../../../../main/webapp/app/entities/age/age-detail.component';
import { AgeService } from '../../../../../../main/webapp/app/entities/age/age.service';
import { Age } from '../../../../../../main/webapp/app/entities/age/age.model';

describe('Component Tests', () => {

    describe('Age Management Detail Component', () => {
        let comp: AgeDetailComponent;
        let fixture: ComponentFixture<AgeDetailComponent>;
        let service: AgeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [AgeDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    AgeService
                ]
            }).overrideComponent(AgeDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AgeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgeService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Age('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.age).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
