import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TrialCentreDetailComponent } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre-detail.component';
import { TrialCentreService } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.service';
import { TrialCentre } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.model';

describe('Component Tests', () => {

    describe('TrialCentre Management Detail Component', () => {
        let comp: TrialCentreDetailComponent;
        let fixture: ComponentFixture<TrialCentreDetailComponent>;
        let service: TrialCentreService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [TrialCentreDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    TrialCentreService
                ]
            }).overrideComponent(TrialCentreDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialCentreDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialCentreService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TrialCentre('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.trialCentre).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
