import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ConceptDetailComponent } from '../../../../../../main/webapp/app/entities/concept/concept-detail.component';
import { ConceptService } from '../../../../../../main/webapp/app/entities/concept/concept.service';
import { Concept } from '../../../../../../main/webapp/app/entities/concept/concept.model';

describe('Component Tests', () => {

    describe('Concept Management Detail Component', () => {
        let comp: ConceptDetailComponent;
        let fixture: ComponentFixture<ConceptDetailComponent>;
        let service: ConceptService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ConceptDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    ConceptService
                ]
            }).overrideComponent(ConceptDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConceptDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConceptService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Concept('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.concept).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
