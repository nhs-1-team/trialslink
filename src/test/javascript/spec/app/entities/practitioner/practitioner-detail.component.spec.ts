import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PractitionerDetailComponent } from '../../../../../../main/webapp/app/entities/practitioner/practitioner-detail.component';
import { PractitionerService } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.service';
import { Practitioner } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.model';

describe('Component Tests', () => {

    describe('Practitioner Management Detail Component', () => {
        let comp: PractitionerDetailComponent;
        let fixture: ComponentFixture<PractitionerDetailComponent>;
        let service: PractitionerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [PractitionerDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    PractitionerService
                ]
            }).overrideComponent(PractitionerDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PractitionerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PractitionerService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Practitioner('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.practitioner).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
