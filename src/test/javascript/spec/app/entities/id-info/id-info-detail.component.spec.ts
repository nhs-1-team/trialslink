import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { IdInfoDetailComponent } from '../../../../../../main/webapp/app/entities/id-info/id-info-detail.component';
import { IdInfoService } from '../../../../../../main/webapp/app/entities/id-info/id-info.service';
import { IdInfo } from '../../../../../../main/webapp/app/entities/id-info/id-info.model';

describe('Component Tests', () => {

    describe('IdInfo Management Detail Component', () => {
        let comp: IdInfoDetailComponent;
        let fixture: ComponentFixture<IdInfoDetailComponent>;
        let service: IdInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [IdInfoDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    IdInfoService
                ]
            }).overrideComponent(IdInfoDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdInfoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdInfoService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new IdInfo('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.idInfo).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
