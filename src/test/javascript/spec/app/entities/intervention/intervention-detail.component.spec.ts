import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { InterventionDetailComponent } from '../../../../../../main/webapp/app/entities/intervention/intervention-detail.component';
import { InterventionService } from '../../../../../../main/webapp/app/entities/intervention/intervention.service';
import { Intervention } from '../../../../../../main/webapp/app/entities/intervention/intervention.model';

describe('Component Tests', () => {

    describe('Intervention Management Detail Component', () => {
        let comp: InterventionDetailComponent;
        let fixture: ComponentFixture<InterventionDetailComponent>;
        let service: InterventionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [InterventionDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    InterventionService
                ]
            }).overrideComponent(InterventionDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(InterventionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InterventionService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Intervention('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.intervention).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
