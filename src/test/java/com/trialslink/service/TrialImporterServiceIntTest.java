package com.trialslink.service;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Trial;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class TrialImporterServiceIntTest {

    private final Logger log = LoggerFactory.getLogger(TrialImporterServiceIntTest.class);

    @Autowired
    private TrialImporterService trialImporterService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testXmlConversion() throws Exception {
        // set up with random id and UKALL14
        String[] ids = {"NCT00001372", "NCT01085617"};

        // test
        Arrays.asList(ids).forEach(item -> {
            Trial trial = null;
            try {
                trial = trialImporterService.importTrialFromClinicalTrailsGov(item);

                // Verify
                assertThat(trial.getIdInfo().getNctId()).isEqualTo(item);
                assertThat(trial.getIdInfo().getSecondaryIds().size()).isGreaterThan(0);
                assertThat(trial.getTitle()).isNotNull();
                assertThat(trial.getBriefTitle()).isNotNull();
                assertThat(trial.getBriefSummary()).isNotNull();
                assertThat(trial.getDetailedDescription()).isNotNull();
                assertThat(trial.getPhases()).isNotNull();
                assertThat(trial.getStatus()).isNotNull();
                assertThat(trial.getType()).isNotNull();
            } catch (IOException e) {
                log.error("Nested exception is : ", e);
            }
        });
    }

    @Test
    public void testXmlConversionSkipsConditionsInterventionsAndLocations() throws Exception {
        // set up with random id and UKALL14
        String[] ids = {"NCT00001372", "NCT01085617"};

        // test
        Arrays.asList(ids).forEach(item -> {
            Trial trial = null;
            try {
                trial = trialImporterService.importTrialFromClinicalTrailsGov(item);

                // Verify
                assertThat(trial.getIdInfo().getNctId()).isEqualTo(item);
                // by default import should skip conditions, interventions and sites
                assertThat(trial.getConditions().isEmpty());
                assertThat(trial.getInterventions().isEmpty());
                assertThat(trial.getSites().isEmpty());
            } catch (IOException e) {
                log.error("Nested exception is : ", e);
            }
        });
    }
}
