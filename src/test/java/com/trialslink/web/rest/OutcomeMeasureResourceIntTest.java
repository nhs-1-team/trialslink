package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.OutcomeMeasure;
import com.trialslink.repository.OutcomeMeasureRepository;
import com.trialslink.service.OutcomeMeasureService;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OutcomeMeasureResource REST controller.
 *
 * @see OutcomeMeasureResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class OutcomeMeasureResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TIME_FRAME = "AAAAAAAAAA";
    private static final String UPDATED_TIME_FRAME = "BBBBBBBBBB";

    @Autowired
    private OutcomeMeasureRepository outcomeMeasureRepository;

    @Autowired
    private OutcomeMeasureService outcomeMeasureService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restOutcomeMeasureMockMvc;

    private OutcomeMeasure outcomeMeasure;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OutcomeMeasureResource outcomeMeasureResource = new OutcomeMeasureResource(outcomeMeasureService);
        this.restOutcomeMeasureMockMvc = MockMvcBuilders.standaloneSetup(outcomeMeasureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OutcomeMeasure createEntity() {
        OutcomeMeasure outcomeMeasure = new OutcomeMeasure()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .timeFrame(DEFAULT_TIME_FRAME);
        return outcomeMeasure;
    }

    @Before
    public void initTest() {
        outcomeMeasureRepository.deleteAll();
        outcomeMeasure = createEntity();
    }

    @Test
    public void createOutcomeMeasure() throws Exception {
        int databaseSizeBeforeCreate = outcomeMeasureRepository.findAll().size();

        // Create the OutcomeMeasure
        restOutcomeMeasureMockMvc.perform(post("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(outcomeMeasure)))
            .andExpect(status().isCreated());

        // Validate the OutcomeMeasure in the database
        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeCreate + 1);
        OutcomeMeasure testOutcomeMeasure = outcomeMeasureList.get(outcomeMeasureList.size() - 1);
        assertThat(testOutcomeMeasure.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testOutcomeMeasure.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testOutcomeMeasure.getTimeFrame()).isEqualTo(DEFAULT_TIME_FRAME);
    }

    @Test
    public void createOutcomeMeasureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = outcomeMeasureRepository.findAll().size();

        // Create the OutcomeMeasure with an existing ID
        outcomeMeasure.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restOutcomeMeasureMockMvc.perform(post("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(outcomeMeasure)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = outcomeMeasureRepository.findAll().size();
        // set the field null
        outcomeMeasure.setTitle(null);

        // Create the OutcomeMeasure, which fails.

        restOutcomeMeasureMockMvc.perform(post("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(outcomeMeasure)))
            .andExpect(status().isBadRequest());

        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = outcomeMeasureRepository.findAll().size();
        // set the field null
        outcomeMeasure.setDescription(null);

        // Create the OutcomeMeasure, which fails.

        restOutcomeMeasureMockMvc.perform(post("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(outcomeMeasure)))
            .andExpect(status().isBadRequest());

        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkTimeFrameIsRequired() throws Exception {
        int databaseSizeBeforeTest = outcomeMeasureRepository.findAll().size();
        // set the field null
        outcomeMeasure.setTimeFrame(null);

        // Create the OutcomeMeasure, which fails.

        restOutcomeMeasureMockMvc.perform(post("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(outcomeMeasure)))
            .andExpect(status().isBadRequest());

        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllOutcomeMeasures() throws Exception {
        // Initialize the database
        outcomeMeasureRepository.save(outcomeMeasure);

        // Get all the outcomeMeasureList
        restOutcomeMeasureMockMvc.perform(get("/api/outcome-measures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(outcomeMeasure.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].timeFrame").value(hasItem(DEFAULT_TIME_FRAME.toString())));
    }

    @Test
    public void getOutcomeMeasure() throws Exception {
        // Initialize the database
        outcomeMeasureRepository.save(outcomeMeasure);

        // Get the outcomeMeasure
        restOutcomeMeasureMockMvc.perform(get("/api/outcome-measures/{id}", outcomeMeasure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(outcomeMeasure.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.timeFrame").value(DEFAULT_TIME_FRAME.toString()));
    }

    @Test
    public void getNonExistingOutcomeMeasure() throws Exception {
        // Get the outcomeMeasure
        restOutcomeMeasureMockMvc.perform(get("/api/outcome-measures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateOutcomeMeasure() throws Exception {
        // Initialize the database
        outcomeMeasureService.save(outcomeMeasure);

        int databaseSizeBeforeUpdate = outcomeMeasureRepository.findAll().size();

        // Update the outcomeMeasure
        OutcomeMeasure updatedOutcomeMeasure = outcomeMeasureRepository.findOne(outcomeMeasure.getId());
        updatedOutcomeMeasure
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .timeFrame(UPDATED_TIME_FRAME);

        restOutcomeMeasureMockMvc.perform(put("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOutcomeMeasure)))
            .andExpect(status().isOk());

        // Validate the OutcomeMeasure in the database
        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeUpdate);
        OutcomeMeasure testOutcomeMeasure = outcomeMeasureList.get(outcomeMeasureList.size() - 1);
        assertThat(testOutcomeMeasure.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testOutcomeMeasure.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testOutcomeMeasure.getTimeFrame()).isEqualTo(UPDATED_TIME_FRAME);
    }

    @Test
    public void updateNonExistingOutcomeMeasure() throws Exception {
        int databaseSizeBeforeUpdate = outcomeMeasureRepository.findAll().size();

        // Create the OutcomeMeasure

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOutcomeMeasureMockMvc.perform(put("/api/outcome-measures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(outcomeMeasure)))
            .andExpect(status().isCreated());

        // Validate the OutcomeMeasure in the database
        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteOutcomeMeasure() throws Exception {
        // Initialize the database
        outcomeMeasureService.save(outcomeMeasure);

        int databaseSizeBeforeDelete = outcomeMeasureRepository.findAll().size();

        // Get the outcomeMeasure
        restOutcomeMeasureMockMvc.perform(delete("/api/outcome-measures/{id}", outcomeMeasure.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OutcomeMeasure> outcomeMeasureList = outcomeMeasureRepository.findAll();
        assertThat(outcomeMeasureList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OutcomeMeasure.class);
    }
}
