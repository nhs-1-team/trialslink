package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.IdInfo;
import com.trialslink.repository.IdInfoRepository;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the IdInfoResource REST controller.
 *
 * @see IdInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class IdInfoResourceIntTest {

    private static final String DEFAULT_PROTOCOL_ID = "AAAAAAAAAA";
    private static final String UPDATED_PROTOCOL_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NCT_ID = "AAAAAAAAAA";
    private static final String UPDATED_NCT_ID = "BBBBBBBBBB";

    @Autowired
    private IdInfoRepository idInfoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restIdInfoMockMvc;

    private IdInfo idInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        IdInfoResource idInfoResource = new IdInfoResource(idInfoRepository);
        this.restIdInfoMockMvc = MockMvcBuilders.standaloneSetup(idInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IdInfo createEntity() {
        IdInfo idInfo = new IdInfo()
            .protocolId(DEFAULT_PROTOCOL_ID)
            .nctId(DEFAULT_NCT_ID);
        return idInfo;
    }

    @Before
    public void initTest() {
        idInfoRepository.deleteAll();
        idInfo = createEntity();
    }

    @Test
    public void createIdInfo() throws Exception {
        int databaseSizeBeforeCreate = idInfoRepository.findAll().size();

        // Create the IdInfo
        restIdInfoMockMvc.perform(post("/api/id-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idInfo)))
            .andExpect(status().isCreated());

        // Validate the IdInfo in the database
        List<IdInfo> idInfoList = idInfoRepository.findAll();
        assertThat(idInfoList).hasSize(databaseSizeBeforeCreate + 1);
        IdInfo testIdInfo = idInfoList.get(idInfoList.size() - 1);
        assertThat(testIdInfo.getProtocolId()).isEqualTo(DEFAULT_PROTOCOL_ID);
        assertThat(testIdInfo.getNctId()).isEqualTo(DEFAULT_NCT_ID);
    }

    @Test
    public void createIdInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = idInfoRepository.findAll().size();

        // Create the IdInfo with an existing ID
        idInfo.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restIdInfoMockMvc.perform(post("/api/id-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idInfo)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<IdInfo> idInfoList = idInfoRepository.findAll();
        assertThat(idInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkProtocolIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = idInfoRepository.findAll().size();
        // set the field null
        idInfo.setProtocolId(null);

        // Create the IdInfo, which fails.

        restIdInfoMockMvc.perform(post("/api/id-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idInfo)))
            .andExpect(status().isBadRequest());

        List<IdInfo> idInfoList = idInfoRepository.findAll();
        assertThat(idInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllIdInfos() throws Exception {
        // Initialize the database
        idInfoRepository.save(idInfo);

        // Get all the idInfoList
        restIdInfoMockMvc.perform(get("/api/id-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(idInfo.getId())))
            .andExpect(jsonPath("$.[*].protocolId").value(hasItem(DEFAULT_PROTOCOL_ID.toString())))
            .andExpect(jsonPath("$.[*].nctId").value(hasItem(DEFAULT_NCT_ID.toString())));
    }

    @Test
    public void getIdInfo() throws Exception {
        // Initialize the database
        idInfoRepository.save(idInfo);

        // Get the idInfo
        restIdInfoMockMvc.perform(get("/api/id-infos/{id}", idInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(idInfo.getId()))
            .andExpect(jsonPath("$.protocolId").value(DEFAULT_PROTOCOL_ID.toString()))
            .andExpect(jsonPath("$.nctId").value(DEFAULT_NCT_ID.toString()));
    }

    @Test
    public void getNonExistingIdInfo() throws Exception {
        // Get the idInfo
        restIdInfoMockMvc.perform(get("/api/id-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateIdInfo() throws Exception {
        // Initialize the database
        idInfoRepository.save(idInfo);
        int databaseSizeBeforeUpdate = idInfoRepository.findAll().size();

        // Update the idInfo
        IdInfo updatedIdInfo = idInfoRepository.findOne(idInfo.getId());
        updatedIdInfo
            .protocolId(UPDATED_PROTOCOL_ID)
            .nctId(UPDATED_NCT_ID);

        restIdInfoMockMvc.perform(put("/api/id-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIdInfo)))
            .andExpect(status().isOk());

        // Validate the IdInfo in the database
        List<IdInfo> idInfoList = idInfoRepository.findAll();
        assertThat(idInfoList).hasSize(databaseSizeBeforeUpdate);
        IdInfo testIdInfo = idInfoList.get(idInfoList.size() - 1);
        assertThat(testIdInfo.getProtocolId()).isEqualTo(UPDATED_PROTOCOL_ID);
        assertThat(testIdInfo.getNctId()).isEqualTo(UPDATED_NCT_ID);
    }

    @Test
    public void updateNonExistingIdInfo() throws Exception {
        int databaseSizeBeforeUpdate = idInfoRepository.findAll().size();

        // Create the IdInfo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restIdInfoMockMvc.perform(put("/api/id-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idInfo)))
            .andExpect(status().isCreated());

        // Validate the IdInfo in the database
        List<IdInfo> idInfoList = idInfoRepository.findAll();
        assertThat(idInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteIdInfo() throws Exception {
        // Initialize the database
        idInfoRepository.save(idInfo);
        int databaseSizeBeforeDelete = idInfoRepository.findAll().size();

        // Get the idInfo
        restIdInfoMockMvc.perform(delete("/api/id-infos/{id}", idInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<IdInfo> idInfoList = idInfoRepository.findAll();
        assertThat(idInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IdInfo.class);
    }
}
