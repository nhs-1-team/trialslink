package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.Practitioner;
import com.trialslink.repository.PractitionerRepository;
import com.trialslink.service.PractitionerService;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.trialslink.domain.enumeration.Gender;
/**
 * Test class for the PractitionerResource REST controller.
 *
 * @see PractitionerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class PractitionerResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    @Autowired
    private PractitionerRepository practitionerRepository;

    @Autowired
    private PractitionerService practitionerService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restPractitionerMockMvc;

    private Practitioner practitioner;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PractitionerResource practitionerResource = new PractitionerResource(practitionerService);
        this.restPractitionerMockMvc = MockMvcBuilders.standaloneSetup(practitionerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Practitioner createEntity() {
        Practitioner practitioner = new Practitioner()
            .name(DEFAULT_NAME)
            .gender(DEFAULT_GENDER);
        return practitioner;
    }

    @Before
    public void initTest() {
        practitionerRepository.deleteAll();
        practitioner = createEntity();
    }

    @Test
    public void createPractitioner() throws Exception {
        int databaseSizeBeforeCreate = practitionerRepository.findAll().size();

        // Create the Practitioner
        restPractitionerMockMvc.perform(post("/api/practitioners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practitioner)))
            .andExpect(status().isCreated());

        // Validate the Practitioner in the database
        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeCreate + 1);
        Practitioner testPractitioner = practitionerList.get(practitionerList.size() - 1);
        assertThat(testPractitioner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPractitioner.getGender()).isEqualTo(DEFAULT_GENDER);
    }

    @Test
    public void createPractitionerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = practitionerRepository.findAll().size();

        // Create the Practitioner with an existing ID
        practitioner.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPractitionerMockMvc.perform(post("/api/practitioners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practitioner)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = practitionerRepository.findAll().size();
        // set the field null
        practitioner.setName(null);

        // Create the Practitioner, which fails.

        restPractitionerMockMvc.perform(post("/api/practitioners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practitioner)))
            .andExpect(status().isBadRequest());

        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = practitionerRepository.findAll().size();
        // set the field null
        practitioner.setGender(null);

        // Create the Practitioner, which fails.

        restPractitionerMockMvc.perform(post("/api/practitioners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practitioner)))
            .andExpect(status().isBadRequest());

        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllPractitioners() throws Exception {
        // Initialize the database
        practitionerRepository.save(practitioner);

        // Get all the practitionerList
        restPractitionerMockMvc.perform(get("/api/practitioners?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(practitioner.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())));
    }

    @Test
    public void getPractitioner() throws Exception {
        // Initialize the database
        practitionerRepository.save(practitioner);

        // Get the practitioner
        restPractitionerMockMvc.perform(get("/api/practitioners/{id}", practitioner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(practitioner.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()));
    }

    @Test
    public void getNonExistingPractitioner() throws Exception {
        // Get the practitioner
        restPractitionerMockMvc.perform(get("/api/practitioners/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePractitioner() throws Exception {
        // Initialize the database
        practitionerService.save(practitioner);

        int databaseSizeBeforeUpdate = practitionerRepository.findAll().size();

        // Update the practitioner
        Practitioner updatedPractitioner = practitionerRepository.findOne(practitioner.getId());
        updatedPractitioner
            .name(UPDATED_NAME)
            .gender(UPDATED_GENDER);

        restPractitionerMockMvc.perform(put("/api/practitioners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPractitioner)))
            .andExpect(status().isOk());

        // Validate the Practitioner in the database
        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeUpdate);
        Practitioner testPractitioner = practitionerList.get(practitionerList.size() - 1);
        assertThat(testPractitioner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPractitioner.getGender()).isEqualTo(UPDATED_GENDER);
    }

    @Test
    public void updateNonExistingPractitioner() throws Exception {
        int databaseSizeBeforeUpdate = practitionerRepository.findAll().size();

        // Create the Practitioner

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPractitionerMockMvc.perform(put("/api/practitioners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(practitioner)))
            .andExpect(status().isCreated());

        // Validate the Practitioner in the database
        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deletePractitioner() throws Exception {
        // Initialize the database
        practitionerService.save(practitioner);

        int databaseSizeBeforeDelete = practitionerRepository.findAll().size();

        // Get the practitioner
        restPractitionerMockMvc.perform(delete("/api/practitioners/{id}", practitioner.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Practitioner> practitionerList = practitionerRepository.findAll();
        assertThat(practitionerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Practitioner.class);
    }
}
